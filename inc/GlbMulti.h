/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMulti.h			*/
/********************************************************/

#ifndef _GLBMULTI_H
#define	_GLBMULTI_H

#include "GlbNet.h"

namespace GlbNet
{

class CGlbMulti : public CGlbNet
{
public:
	CGlbMulti();
	virtual ~CGlbMulti();

	void* GlbMultiGetMulti();
	void GlbMultiSetMulti(void* pAddr);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength);
	virtual int GlbNetSendAll(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecvAll(UCHAR* puszPacket, int iLength);

	virtual int GlbNetInitSend(char* pszUrl);
	virtual int GlbNetInitRecv(char* pszUrl);
	virtual int GlbNetInitSend(struct sockaddr_in* pstAddr);
	virtual int GlbNetInitRecv(struct sockaddr_in* pstAddr);
	virtual int GlbNetInitSend(char* pszAddress, char* pszPort);
	virtual int GlbNetInitRecv(char* pszAddress, char* pszPort);

protected:
	struct sockaddr_in m_stMulti;

private:

};

inline int CGlbMulti::GlbNetSend(UCHAR* puszPacket, int iLength)
{
	return sendto(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stMulti, sizeof(struct sockaddr_in));
}

inline int CGlbMulti::GlbNetRecv(UCHAR* puszPacket, int iLength)
{
	int iMulti = sizeof(struct sockaddr_in);

	return recvfrom(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stMulti, (socklen_t*)&iMulti);
}

} /* GlbNet */

#endif /* _GLBMULTI_H */
