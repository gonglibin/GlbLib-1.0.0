/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbEpoll.h			*/
/********************************************************/

#ifndef _GLBEPOLL_H
#define	_GLBEPOLL_H

#include "GlbTcp.h"
#include "GlbUdp.h"
#include <sys/epoll.h>

namespace GlbNet
{

#define	GLB_EVENT			256
#define	GLB_EPOLL			50000

class CGlbEpoll
{
public:
	CGlbEpoll();
	virtual ~CGlbEpoll();

	void GlbEpollClose();
	int GlbEpollCreate();
	int GlbEpollGetEpoll();
	void GlbEpollUdpWait();
	void GlbEpollTcpWait();
	void GlbEpollCloseNet();
	CGlbNet* GlbEpollGetNet();
	int GlbEpollGlblAdd(int iSocket);
	int GlbEpollGlblDel(int iSocket);
	void GlbEpollPutEpoll(int iEpoll);
	void GlbEpollPutNet(CGlbNet* pCNet);
	int GlbEpollUdpInit(char* pszPort);
	int GlbEpollTcpInit(char* pszPort);
	void GlbEpollSetFunction(int (*pFunction)(int));
	int GlbEpollGlblAdd(struct epoll_event* pstEpoll);
	int GlbEpollGlblDel(struct epoll_event* pstEpoll);
	int GlbEpollWait(struct epoll_event* pstEvent, int iEvent);

private:
	int m_iEpoll;
	CGlbNet* m_pCNet;
	int (*m_pFunction)(int);

};

inline int CGlbEpoll::GlbEpollGlblAdd(int iSocket)
{
	struct epoll_event stEpoll;

	stEpoll.data.fd = iSocket;
	stEpoll.events = EPOLLIN | EPOLLET;

	return epoll_ctl(m_iEpoll, EPOLL_CTL_ADD, iSocket, &stEpoll);
}

inline int CGlbEpoll::GlbEpollGlblDel(int iSocket)
{
	struct epoll_event stEpoll;

	stEpoll.data.fd = iSocket;

	return epoll_ctl(m_iEpoll, EPOLL_CTL_DEL, iSocket, &stEpoll);
}

inline int CGlbEpoll::GlbEpollGlblAdd(struct epoll_event* pstEpoll)
{
	return epoll_ctl(m_iEpoll, EPOLL_CTL_ADD, pstEpoll->data.fd, pstEpoll);
}

inline int CGlbEpoll::GlbEpollGlblDel(struct epoll_event* pstEpoll)
{
	return epoll_ctl(m_iEpoll, EPOLL_CTL_DEL, pstEpoll->data.fd, pstEpoll);
}

inline int CGlbEpoll::GlbEpollWait(struct epoll_event* pstEvent, int iEvent)
{
	return epoll_wait(m_iEpoll, pstEvent, iEvent, -1);
}

} /* GlbNet */

#endif /* _GLBEPOLL_H */
