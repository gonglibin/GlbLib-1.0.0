/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMd5Test.cpp			*/
/********************************************************/

#include "GlbMd5.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbMd5 CTd5;
	int iCount = 0;
	UCHAR uszDst[GLB_MD5] = { 0 };
	char szSrc[GLB_PACKET] = { 0 };

	if (NULL == argv[1]) {
		strcpy(szSrc, "Copyright (C) 2016 Gong Li Bin");
	}
	else {
		strcpy(szSrc, argv[1]);
	}

	/*CTd5.GlbMd5Init();
	CTd5.GlbMd5Update((UCHAR*)szSrc, strlen(szSrc));
	CTd5.GlbMd5Final(uszDst);*/
	CTd5.GlbMd5Run((UCHAR*)szSrc, strlen(szSrc), uszDst);

	GLB_PRINT("%s -> ", szSrc);
	for (iCount = 0; iCount < GLB_MD5; iCount ++) {
		GLB_PRINT("%02X", uszDst[iCount]);
	}
	GLB_PRINT("\n");

	GLB_PRINT("UCHAR[00 - 07]: %lu\n", *(ULONG*)uszDst);
	GLB_PRINT("UCHAR[08 - 15]: %lu\n", *(ULONG*)(uszDst + 7));

	GLB_PRINT("[UCHAR1 ^ UCHAR2]: %lu\n", *(ULONG*)uszDst ^ *(ULONG*)(uszDst + GLB_MD5 / 2));

	return GLB_SUCCESS;
}
