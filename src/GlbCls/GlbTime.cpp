/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbTime.cpp			*/
/********************************************************/

#include "GlbTime.h"

namespace GlbCls
{

CGlbTime::CGlbTime()
{
	memset(&m_stOn, '\0', sizeof(struct timeval));
	memset(&m_stOff, '\0', sizeof(struct timeval));
}

CGlbTime::~CGlbTime()
{
}

void CGlbTime::GlbTimeOn()
{
	gettimeofday(&m_stOn, NULL);

	return;
}

void CGlbTime::GlbTimeOff()
{
	gettimeofday(&m_stOff, NULL);

	return;
}

void* CGlbTime::GlbTimeGetOn()
{
	return (void*)&m_stOn;
}

void* CGlbTime::GlbTimeGetOff()
{
	return (void*)&m_stOff;
}

void CGlbTime::GlbTimeDisplay()
{
	if (m_stOff.tv_usec == m_stOn.tv_usec) {
		GLB_PRINT("%ld second 0 microsecond\n", m_stOff.tv_sec - m_stOn.tv_sec);
	}
	else if (m_stOff.tv_usec > m_stOn.tv_usec) {
		GLB_PRINT("%ld second %ld microsecond\n", m_stOff.tv_sec - m_stOn.tv_sec, m_stOff.tv_usec - m_stOn.tv_usec);
	}
	else {
		GLB_PRINT("%ld second %ld microsecond\n", m_stOff.tv_sec - m_stOn.tv_sec - 1, 1000000L - m_stOn.tv_usec + m_stOff.tv_usec);
	}

	return;
}

void CGlbTime::GlbTimeDisplayOnNumber()
{
	GLB_PRINT("%ld second %ld microsecond\n", m_stOn.tv_sec, m_stOn.tv_usec);

	return;
}

void CGlbTime::GlbTimeDisplayOnString()
{
	struct tm* pstTime = NULL;
	char szTime[GLB_BUFFER] = { 0 };

	pstTime = localtime(&m_stOn.tv_sec);
	strftime(szTime, GLB_BUFFER, "%H:%M:%S", pstTime);
	GLB_PRINT("Time: %s.%ld\n", szTime, m_stOn.tv_usec);

	return;
}

void CGlbTime::GlbTimeDisplayOffNumber()
{
	GLB_PRINT("%ld second %ld microsecond\n", m_stOff.tv_sec, m_stOff.tv_usec);

	return;
}

void CGlbTime::GlbTimeDisplayOffString()
{
	struct tm* pstTime = NULL;
	char szTime[GLB_BUFFER] = { 0 };

	pstTime = localtime(&m_stOff.tv_sec);
	strftime(szTime, GLB_BUFFER, "%H:%M:%S", pstTime);
	GLB_PRINT("Time: %s.%ld\n", szTime, m_stOff.tv_usec);

	return;
}

} /* GlbCls */
