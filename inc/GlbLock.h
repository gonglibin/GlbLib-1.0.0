/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbLock.h			*/
/********************************************************/

#ifndef _GLBLOCK_H
#define	_GLBLOCK_H

#include "GlbGlobal.h"
#include <pthread.h>

namespace GlbCls
{

class CGlbLock
{
public:
	CGlbLock() { m_pLock = NULL; }
	virtual ~CGlbLock() { m_pLock = NULL; }

	virtual void GlbLockFree()
	{
		if (NULL != m_pLock) {
			free(m_pLock);
			m_pLock = NULL;
		}

		return;
	}

	virtual int GlbLockUnlock() = 0;
	virtual int GlbLockDelete() = 0;
	virtual int GlbLockMalloc(void* pLock) = 0;
	virtual int GlbLockCreate(bool bShared) = 0;
	virtual int GlbLockLock() { return GLB_SUCCESS; }
	virtual void* GlbLockGetLock() { return m_pLock; }

protected:
	void* m_pLock;

private:

};

} /* GlbCls */

#endif /* _GLBLOCK_H */
