/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbWorkerTest.cpp		*/
/********************************************************/

#include "GlbWorker.h"

#include <unistd.h>

using namespace GlbCls;

#define	GLB_WORKER			10

void* GlbWorkerTestCore(void* pPara)
{
	char szPacket[GLB_BYTE16] = { 0 };
	CGlbWorker* pCWorker = (CGlbWorker*)pPara;

	while (true) {
		pCWorker->GlbLockLock();
		if (NULL != pCWorker->GlbWorkerPara()) {
			memcpy(szPacket, pCWorker->GlbWorkerPara(), GLB_BYTE16);
			GLB_PRINT("Thread[%lu]: %s\n", pthread_self(), szPacket);
			usleep(100000);
			pCWorker->GlbWorkerRecycle();
		}
	}

	pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
	UINT uiCount = 0;
	UINT uiValue = 100;
	bool bBreak = false;
	CGlbWorker CWorker[GLB_WORKER];
	char szPacket[GLB_BYTE16] = { 0 };

	while (uiCount < GLB_WORKER) {
		CWorker[uiCount].GlbThreadAttrInit();
		CWorker[uiCount].GlbThreadRoutineSet(GlbWorkerTestCore);
		if (GLB_SUCCESS != CWorker[uiCount].GlbWorkerCreate(&CWorker[uiCount])) {
			GLB_ERROR("Failed to GlbWorkerCreate\n");
			return GLB_FAILURE;
		}
		uiCount ++;
	}

	uiCount = 0;
	while (true) {
		bBreak = false;
		sprintf(szPacket, "%d", uiValue ++);
		while (false == bBreak) {
			if (GLB_SUCCESS == CWorker[uiCount].GlbWorkerAppoint(szPacket)) {
				bBreak = true;
			}
			GLB_WORKER == uiCount ? uiCount = 0 : uiCount ++;
		}
	}

	return GLB_SUCCESS;
}
