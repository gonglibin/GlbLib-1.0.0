/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbThash.h			*/
/********************************************************/

#ifndef _GLBTHASH_H
#define	_GLBTHASH_H

#include "GlbHash.h"
#include "GlbMtLock.h"

using namespace GlbCls;

namespace GlbIpc
{

class CGlbThash : public CGlbHash
{
public:
	CGlbThash();
	virtual ~CGlbThash();

	int GlbThashLock();
	int GlbThashUnlock();
	void GlbThashDelete();
	int GlbThashCreate(key_t iKey, ULONG ulSize);

protected:
	CGlbMtLock m_CLock;

private:

};

inline int CGlbThash::GlbThashLock()
{
	return m_CLock.GlbLockLock();
}

inline int CGlbThash::GlbThashUnlock()
{
	return m_CLock.GlbLockUnlock();
}

} /* GlbIpc */

#endif /* _GLBHASH_H */
