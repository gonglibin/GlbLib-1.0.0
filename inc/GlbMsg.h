/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMsg.h			*/
/********************************************************/

#ifndef _GLBMSG_H
#define	_GLBMSG_H

#include "GlbIpc.h"
#include <sys/msg.h>

namespace GlbIpc
{

class CGlbMsg : public CGlbIpc
{
public:
	CGlbMsg();
	virtual ~CGlbMsg();

	int GlbMsgGet(void* pMsg, UINT uiSize, int iFlag);
	int GlbMsgPut(void* pMsg, UINT uiSize, int iFlag);

	virtual int GlbIpcDelete();
	virtual int GlbIpcCreate(key_t iKey);

protected:

private:

};

inline int CGlbMsg::GlbMsgPut(void* pMsg, UINT uiSize, int iFlag)
{
	return msgsnd(CGlbIpc::m_iIpc, pMsg, uiSize, iFlag);
}

inline int CGlbMsg::GlbMsgGet(void* pMsg, UINT uiSize, int iFlag)
{
	return msgrcv(CGlbIpc::m_iIpc, pMsg, uiSize, 0, iFlag);
}

} /* GlbIpc */

#endif /* _GLBMSG_H */
