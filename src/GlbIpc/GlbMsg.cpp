/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMsg.cpp			*/
/********************************************************/

#include "GlbMsg.h"

namespace GlbIpc
{

CGlbMsg::CGlbMsg()
{
}

CGlbMsg::~CGlbMsg()
{
}

int CGlbMsg::GlbIpcDelete()
{
	return msgctl(CGlbIpc::m_iIpc, IPC_RMID, 0);
}

int CGlbMsg::GlbIpcCreate(key_t iKey)
{
	if (GLB_FAILURE == (CGlbIpc::m_iIpc = msgget(iKey, IPC_CREAT | 0777))) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

} /* GlbIpc */
