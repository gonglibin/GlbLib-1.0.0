/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbBaseTest.cpp			*/
/********************************************************/

#include "GlbUtil.h"

int main(int argc, char* argv[])
{
	int iDst = 0;
	UCHAR* puszDst = NULL;
	UCHAR* puszBase = NULL;
	UCHAR uszSrc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	if (NULL != (puszBase = GlbBaseEncode((UCHAR*)uszSrc, strlen((char*)uszSrc), &iDst))) {
		GLB_PRINT("Encode:\n");
		GLB_PRINT("Src[%d]: %s\n", strlen((char*)uszSrc), (char*)uszSrc);
		GLB_PRINT("Dst[%d]: %s\n", strlen((char*)puszBase), (char*)puszBase);
		if (NULL != (puszDst = GlbBaseDecode(puszBase, strlen((char*)puszBase), &iDst))) {
			GLB_PRINT("Decode:\n");
			GLB_PRINT("Src[%d]: %s\n", strlen((char*)puszBase), (char*)puszBase);
			GLB_PRINT("Dst[%d]: %s\n", strlen((char*)puszDst), (char*)puszDst);
			GlbFree((void**)&puszDst);
		}
		GlbFree((void**)&puszBase);
	}

	return GLB_SUCCESS;
}
