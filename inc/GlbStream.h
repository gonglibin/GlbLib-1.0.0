/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbStream.h			*/
/********************************************************/

#ifndef _GLBSTREAM_H
#define	_GLBSTREAM_H

#include "GlbNet.h"

namespace GlbNet
{

#define	GLB_BACKLOG			20

class CGlbStream : public CGlbNet
{
public:
	CGlbStream();
	virtual ~CGlbStream();

	void GlbStreamAccept();
	void GlbStreamSetFunction(void (*pFunction)(int));

	int GlbStreamInit(char* pszUrl);
	int GlbStreamInit(struct sockaddr_un* pstAddr);
	int GlbStreamOpenBind(struct sockaddr_un* pstAddr);

	int GlbStreamConnect(char* pszUrl);
	int GlbStreamConnect(struct sockaddr_un* pstAddr);

	int GlbStreamRead(UCHAR* puszPacket, UINT uiLength);
	int GlbStreamWrite(UCHAR* puszPacket, UINT uiLength);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength);

private:
	void (*m_pFunction)(int);

};

inline int CGlbStream::GlbStreamRead(UCHAR* puszPacket, UINT uiLength)
{
	return read(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbStream::GlbStreamWrite(UCHAR* puszPacket, UINT uiLength)
{
	return write(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbStream::GlbNetSend(UCHAR* puszPacket, int iLength)
{
	return send(CGlbNet::m_iSocket, puszPacket, iLength, 0);
}

inline int CGlbStream::GlbNetRecv(UCHAR* puszPacket, int iLength)
{
	return recv(CGlbNet::m_iSocket, puszPacket, iLength, 0);
}

} /* GlbNet */

#endif /* _GLBSTREAM_H */
