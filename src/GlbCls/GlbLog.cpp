/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbLog.cpp			*/
/********************************************************/

#include "GlbLog.h"

namespace GlbCls
{

CGlbLog::CGlbLog()
{
	m_pstLog = NULL;
}

CGlbLog::~CGlbLog()
{
	m_pstLog = NULL;
}

void CGlbLog::GlbLogFlush()
{
	fflush(m_pstLog);

	return;
}

FILE* CGlbLog::GlbLogGetLog()
{
	return m_pstLog;
}

void CGlbLog::GlbLogClose()
{
	if (NULL != m_pstLog) {
		fclose(m_pstLog);
		m_pstLog = NULL;
	}

	return;
}

int CGlbLog::GlbLogOpen(char* pszLog, char* pszMode)
{
	if (NULL == (m_pstLog = fopen(pszLog, pszMode))) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

} /* GlbCls */
