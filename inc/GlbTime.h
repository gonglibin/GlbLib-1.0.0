/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbTime.h			*/
/********************************************************/

#ifndef _GLBTIME_H
#define	_GLBTIME_H

#include "GlbGlobal.h"
#include <sys/time.h>
#include <time.h>

namespace GlbCls
{

class CGlbTime
{
public:
	CGlbTime();
	virtual ~CGlbTime();

	void GlbTimeOn();
	void GlbTimeOff();
	void* GlbTimeGetOn();
	void* GlbTimeGetOff();
	void GlbTimeDisplay();
	void GlbTimeDisplayOnNumber();
	void GlbTimeDisplayOnString();
	void GlbTimeDisplayOffNumber();
	void GlbTimeDisplayOffString();

private:
	struct timeval m_stOn;
	struct timeval m_stOff;

};

} /* GlbCls */

#endif /* _GLBTIME_H */
