/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbConfTest.cpp			*/
/********************************************************/

#include "GlbConf.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbConf CConf;
	char* pszKey = NULL;
	char* pszVal = NULL;
	char szLine[GLB_PACKET] = { 0 };
	char szFile[] = "/usr/local/bin/GlbConfig.conf";

	if (GLB_SUCCESS == CConf.GlbConfOpen(szFile)) {
		while (GLB_SUCCESS == CConf.GlbConfGetLine(szLine, GLB_PACKET)) {
			CConf.GlbConfTrim(szLine);
			if (GLB_SUCCESS == CConf.GlbConfKeyValue(szLine, &pszKey, &pszVal)) {
				GLB_PRINT("[KEY]: %s = [VAL]: %s\n", pszKey, pszVal);
			}
			memset(szLine, '\0', GLB_PACKET);
		}
		CConf.GlbConfClose();
	}

	return GLB_SUCCESS;
}
