/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbShm.h			*/
/********************************************************/

#ifndef _GLBSHM_H
#define	_GLBSHM_H

#include "GlbIpc.h"
#include <sys/shm.h>

namespace GlbIpc
{

class CGlbShm : public CGlbIpc
{
public:
	CGlbShm();
	virtual ~CGlbShm();

	int GlbShmLock();
	int GlbShmUnlock();
	void GlbShmDetach();
	void* GlbShmGetShm();
	void* GlbShmAttach();
	void GlbShmReset(ULONG ulSize);

	virtual int GlbIpcDelete();
	virtual int GlbIpcCreate(key_t iKey, ULONG ulSize);

protected:
	void* m_pShm;

private:

};

} /* GlbIpc */

#endif /* _GLBSHM_H */
