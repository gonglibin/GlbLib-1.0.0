/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDll.h			*/
/********************************************************/

#ifndef _GLBDLL_H
#define	_GLBDLL_H

#include "GlbGlobal.h"
#include <dlfcn.h>

namespace GlbCls
{

#define	GLB_RTLD_NOW			RTLD_NOW
#define	GLB_RTLD_LAZY			RTLD_LAZY

class CGlbDll
{
public:
	CGlbDll();
	virtual ~CGlbDll();

	void* GlbDllGet();
	void GlbDllClose();
	char* GlbDllError();
	void GlbDllPut(void* pDll);
	void* GlbDllFunGet(char* pszFun);
	int GlbDllOpen(char* pszFile, int iMode);

protected:
	void* m_pDll;

private:

};

} /* GlbCls */

#endif /* _GLBDLL_H */
