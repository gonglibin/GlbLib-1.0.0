/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDgramTest.cpp		*/
/********************************************************/

#include "GlbDgram.h"

using namespace GlbNet;

#define	GLB_DT_URL			(char*)"/tmp/GlbUnix.sock"

void GlbDgramTestSend()
{
	CGlbDgram CDgram;
	UINT uiCount = 0;
	char szPacket[] = "This is a test about AF_UNIX";

	if (GLB_SUCCESS == CDgram.GlbNetInitSend(GLB_DT_URL)) {
		while (uiCount < 100) {
			if (CDgram.GlbNetSend((UCHAR*)szPacket, strlen(szPacket)) > 0) {
				GLB_PRINT("Send[%d]: %s\n", uiCount, szPacket);
			}
			else {
				GLB_ERROR("Failed to GlbNetSend[%d]\n", uiCount);
			}
			uiCount ++;
		}
		CDgram.GlbNetClose();
	}

	return;
}

void GlbDgramTestRecv()
{
	CGlbDgram CDgram;
	UINT uiCount = 0;
	char szPacket[GLB_PACKET] = { 0 };

	unlink(GLB_DT_URL);

	if (GLB_SUCCESS == CDgram.GlbNetInitRecv(GLB_DT_URL)) {
		while ((CDgram.GlbNetRecv((UCHAR*)szPacket, GLB_PACKET)) > 0) {
			GLB_PRINT("Recv[%d]: %s\n", uiCount ++, szPacket);
		}
		CDgram.GlbNetClose();
		unlink(GLB_DT_URL);
	}

	return;
}

int main(int argc, char* argv[])
{
	if (argc < 2) {
		GLB_PRINT("Usage: %s [send | recv]\n", argv[0]);
		return GLB_SUCCESS;
	}

	switch (strcmp(argv[1], "send")) {
		case 0:
			GlbDgramTestSend();
			break;
		default:
			GlbDgramTestRecv();
			break;
	}

	return GLB_SUCCESS;
}
