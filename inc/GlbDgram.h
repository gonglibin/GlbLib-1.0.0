/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDgram.h			*/
/********************************************************/

#ifndef _GLBDGRAM_H
#define	_GLBDGRAM_H

#include "GlbNet.h"

namespace GlbNet
{

class CGlbDgram : public CGlbNet
{
public:
	CGlbDgram();
	virtual ~CGlbDgram();

	void GlbDgramReset();
	void* GlbDgramGetAddr();
	void GlbDgramSetAddr(void* pAddr);
	void GlbDgramSetAddr(char* pszUrl);

	int GlbDgramConnect();
	int GlbDgramOpenBind();
	int GlbDgramConnect(char* pszUrl);
	int GlbDgramConnect(struct sockaddr_un* pstAddr);

	int GlbDgramRead(UCHAR* puszPacket, UINT uiLength);
	int GlbDgramWrite(UCHAR* puszPacket, UINT uiLength);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength);

	virtual int GlbNetInitSend(char* pszUrl);
	virtual int GlbNetInitRecv(char* pszUrl);
	virtual int GlbNetInitSend(struct sockaddr_un* pstAddr);
	virtual int GlbNetInitRecv(struct sockaddr_un* pstAddr);

protected:
	struct sockaddr_un m_stAddr;

private:

};

inline int CGlbDgram::GlbDgramRead(UCHAR* puszPacket, UINT uiLength)
{
	return read(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbDgram::GlbDgramWrite(UCHAR* puszPacket, UINT uiLength)
{
	return write(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbDgram::GlbNetSend(UCHAR* puszPacket, int iLength)
{
	return sendto(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stAddr, sizeof(struct sockaddr_un));
}

inline int CGlbDgram::GlbNetRecv(UCHAR* puszPacket, int iLength)
{
	int iDgram = sizeof(struct sockaddr_un);

	return recvfrom(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stAddr, (socklen_t*)&iDgram);
}

} /* GlbNet */

#endif /* _GLBDGRAM_H */
