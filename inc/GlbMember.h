/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMember.h			*/
/********************************************************/

#ifndef _GLBMEMBER_H
#define	_GLBMEMBER_H

#include "GlbThread.h"
#include "GlbCdLock.h"

namespace GlbCls
{

#define	GLB_IDLE			0x00
#define	GLB_BUSY			0x01

class CGlbMember : public CGlbThread
{
public:
	CGlbMember();
	virtual ~CGlbMember();

	void GlbMemberLock();
	void GlbMemberWait();
	void GlbMemberSignal();
	void GlbMemberUnlock();
	void* GlbMemberKeyGet();
	UCHAR GlbMemberStatusGet();

	int GlbMemberCreate();
	int GlbMemberAssign(void* pKey);
	void GlbMemberKeySet(void* pKey);
	void GlbMemberStatusSet(UCHAR ucStatus);

protected:
	void* m_pKey;
	UCHAR m_ucStatus;

private:
	pthread_cond_t m_stCond;
	pthread_mutex_t m_stLock;

};

inline void CGlbMember::GlbMemberLock()
{
	pthread_mutex_lock(&m_stLock);

	return;
}

inline void CGlbMember::GlbMemberUnlock()
{
	pthread_mutex_unlock(&m_stLock);

	return;
}

inline void CGlbMember::GlbMemberWait()
{
	pthread_cond_wait(&m_stCond, &m_stLock);

	return;
}

inline void CGlbMember::GlbMemberSignal()
{
	pthread_cond_signal(&m_stCond);

	return;
}

inline void* CGlbMember::GlbMemberKeyGet()
{
	return m_pKey;
}

inline UCHAR CGlbMember::GlbMemberStatusGet()
{
	return m_ucStatus;
}

inline void CGlbMember::GlbMemberKeySet(void* pKey)
{
	m_pKey = pKey;

	return;
}

inline void CGlbMember::GlbMemberStatusSet(UCHAR ucStatus)
{
	m_ucStatus = ucStatus;

	return;
}

void* GlbMemberRoutine(void* pSelf);

} /* GlbCls */

#endif /* _GLBMEMBER_H */
