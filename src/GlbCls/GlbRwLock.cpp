/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbRwLock.cpp			*/
/********************************************************/

#include "GlbRwLock.h"

namespace GlbCls
{

CGlbRwLock::CGlbRwLock()
{
}

CGlbRwLock::~CGlbRwLock()
{
}

int CGlbRwLock::GlbLockMalloc(void* pLock)
{
	if (NULL == pLock) {
		CGlbLock::m_pLock = (pthread_rwlock_t*)malloc(sizeof(pthread_rwlock_t));
		if (NULL == CGlbLock::m_pLock) {
			return GLB_FAILURE;
		}
	}
	else {
		CGlbLock::m_pLock = (pthread_rwlock_t*)pLock;
	}

	return GLB_SUCCESS;
}

int CGlbRwLock::GlbLockCreate(bool bShared)
{
	pthread_rwlockattr_t stAttr;

	if (NULL != CGlbLock::m_pLock) {
		if (GLB_SUCCESS !=  pthread_rwlockattr_init(&stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}

		if (true == bShared) {
			if (GLB_SUCCESS != pthread_rwlockattr_setpshared(&stAttr, PTHREAD_PROCESS_SHARED)) {
				GLB_ERROR("%s\n", strerror(errno));
				return GLB_FAILURE;
			}
		}

		if (GLB_SUCCESS != pthread_rwlock_init((pthread_rwlock_t*)CGlbLock::m_pLock, &stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}

		if (GLB_SUCCESS != pthread_rwlockattr_destroy(&stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}
	}
	else {
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbRwLock::GlbLockDelete()
{
	if (NULL != CGlbLock::m_pLock) {
		if (GLB_SUCCESS != pthread_rwlock_destroy((pthread_rwlock_t*)CGlbLock::m_pLock)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}
	}

	return GLB_SUCCESS;
}

int CGlbRwLock::GlbLockLock(UCHAR ucType)
{
	int iReturn = GLB_FAILURE;

	switch (ucType) {
		case GLB_RW_RDLK:
			iReturn = GlbRwLockReadLock();
			break;
		case GLB_RW_WRLK:
			iReturn = GlbRwLockWriteLock();
			break;
		default:
			break;
	}

	return iReturn;
}

int CGlbRwLock::GlbLockTryLock(UCHAR ucType)
{
	int iReturn = GLB_FAILURE;

	switch (ucType) {
		case GLB_RW_RDLK:
			iReturn = GlbRwLockTryReadLock();
			break;
		case GLB_RW_WRLK:
			iReturn = GlbRwLockTryWriteLock();
			break;
		default:
			break;
	}

	return iReturn;
}

} /* GlbCls */
