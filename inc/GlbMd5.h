/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMd5.h			*/
/********************************************************/

#ifndef _GLBMD5_H
#define	_GLBMD5_H

#include "GlbGlobal.h"

namespace GlbCls
{

#define	GLB_MD5					16

#define	F1(x, y, z)				(z ^ (x & (y ^ z)))
#define	F2(x, y, z)				F1(z, x, y)
#define	F3(x, y, z)				(x ^ y ^ z)
#define	F4(x, y, z)				(y ^ (x | ~z))

#define	GLBMD5STEP(f, w, x, y, z, data, s)	(w += f(x, y, z) + data, w = w << s | w >> (32 - s), w += x)

class CGlbMd5
{
public:
	CGlbMd5();
	virtual ~CGlbMd5();

	void GlbMd5Init();
	void GlbMd5Final(UCHAR digest[16]);
	void GlbMd5Update(UCHAR* puszBuf, UINT uiLen);
	void GlbMd5Run(UCHAR* puszBuf, UINT uiLen, UCHAR digest[16]);

private:
	UINT m_uiBuf[4];
	UINT m_uiBits[2];
	UCHAR m_ucIn[64];

	void GlbMd5Transform(UINT buf[4], UINT in[16]);
	void GlbMd5ByteReverse(UCHAR* puszBuf, UINT uiLongs);

};

} /* GlbCls */

#endif /* _GLBMD5_H */
