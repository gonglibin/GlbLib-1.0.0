/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDef.h			*/
/********************************************************/

#ifndef _GLBDEF_H
#define	_GLBDEF_H

/* verb */
#define	GLB_DEF_CRTE			0x01		// create
#define	GLB_DEF_DTRY			0x02		// destroy
#define	GLB_DEF_SELT			0x03		// select
#define	GLB_DEF_INST			0x04		// insert
#define	GLB_DEF_DELT			0x05		// delete
#define	GLB_DEF_UPDT			0x06		// update

/* attr */
#define	GLB_DEF_REQU			0x01		// request
#define	GLB_DEF_RESP			0x02		// response

/* rest */
#define	GLB_DEF_SUCC			0x00		// success
#define	GLB_DEF_NULL			0x01		// null
#define	GLB_DEF_MALC			0x02		// malloc
#define	GLB_DEF_TYPE			0x03		// type
#define	GLB_DEF_VERB			0x04		// verb
#define	GLB_DEF_ATTR			0x05		// attr
#define	GLB_DEF_TOTA			0x06		// total
#define	GLB_DEF_LENG			0x07		// length
#define	GLB_DEF_EXPA			0x08		// expand
#define	GLB_DEF_KYER			0x09		// key error
#define	GLB_DEF_VLER			0x10		// val error

#define	GLB_DEF_CONN			0x11		// sock connect
#define	GLB_DEF_SEND			0x12		// sock send
#define	GLB_DEF_RECV			0x13		// sock recv

#define	GLB_DEF_FLNW			0x20		// file truncate
#define	GLB_DEF_FLOP			0x21		// file open
#define	GLB_DEF_FLMP			0x22		// file mmap
#define	GLB_DEF_FLRD			0x23		// file read
#define	GLB_DEF_FLWR			0x24		// file wirte
#define	GLB_DEF_FLER			0x25		// file error

#define	GLB_DEF_LKNW			0x30		// lock new
#define	GLB_DEF_LKLK			0x31		// lock lock
#define	GLB_DEF_LKUL			0x32		// lock unlock
#define	GLB_DEF_LKER			0x33		// lock error

#define	GLB_DEF_UNKN			0xff		// unknown
#define	GLB_DEF_FULL			0xff		// full

#endif /* _GLBDEF_H */
