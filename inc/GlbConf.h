/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbConf.h			*/
/********************************************************/

#ifndef	_GLBCONF_H
#define	_GLBCONF_H

#include "GlbGlobal.h"

namespace GlbCls
{

#define	GLB_EOPN				1
#define	GLB_ESEC				2
#define	GLB_EKEY				3
#define	GLB_EVAL				4
#define	GLB_ECLS				5
#define	GLB_EOTH				6

#define	GLB_GLBLT				'\t'
#define	GLB_GLBLR				'\r'
#define	GLB_GLBLN				'\n'

#define	GLB_SECL				'['
#define	GLB_SECR				']'

#define	GLB_BLANK			' '
#define	GLB_EQUAL			'='
#define	GLB_RCTARK			'#'

class CGlbConf
{
public:
	CGlbConf();
	virtual ~CGlbConf();

	void GlbConfClose();
	int GlbConfOpen(char* pszFile);
	int GlbConfGetSec(char* pszSec);
	void GlbConfTrim(char* pszLine);
	bool GlbConfIsValid(char* pszLine);
	bool GlbConfIsSection(char* pszLine);
	int GlbConfGetLine(char* pszLine, int iSize);
	int GlbConfGetKey(char* pszSec, char* pszKey);
	int GlbConfGetVal(char* pszSec, char* pszKey, char* pszVal);
	int GlbConfKeyValue(char* pszLine, char* pszKey, char* pszVal);
	int GlbConfKeyValue(char* pszLine, char** ppszKey, char** ppszVal);

protected:
	FILE* m_pstFile;

private:
	void GlbConfTrimLeft(char* pszLine);
	void GlbConfTrimRight(char* pszLine);

};

} /* GlbCls */

#endif /* _GLBCONF_H */
