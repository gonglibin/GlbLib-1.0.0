/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbProTest.cpp			*/
/********************************************************/

#include "GlbPro.h"
#include "GlbNet.h"

using namespace GlbNet;

int main(int argc, char* argv[])
{
	CGlbPro CPro1;
	CGlbPro CPro2;
	GLBPROKV_S stKv;
	int iLength = 0;
	GLBPROKV_S* pstKv = NULL;
	UCHAR uszPacket[GLB_PACKET] = { 0 };
	char szBody[] = "This is CGlbPro test!";

	/* create */
	stKv.m_ucKey = 0x01;
	stKv.m_iLength = strlen(szBody);
	stKv.m_puszValue = (UCHAR*)szBody;
	iLength = GlbProKvOutput(&stKv, 1, &uszPacket[GLB_PRO_HEAD], GLB_ETHERNET - GLB_MACIPTCP);

	CPro1.GlbProPutFlag(0xe8);
	CPro1.GlbProPutVersion(0x01);
	CPro1.GlbProPutProtocol(0x01);
	CPro1.GlbProPutWide(0x00);
	CPro1.GlbProPutResponse(0x00);
	CPro1.GlbProPutExpand(0x00);
	CPro1.GlbProPutVerb(0x08);
	CPro1.GlbProPutType(0x00);
	CPro1.GlbProPutAttr(0x00);
	CPro1.GlbProPutResult(0x80);
	CPro1.GlbProPutBody(NULL);
	CPro1.GlbProPutTotal(GLB_PRO_1);
	CPro1.GlbProPutLength(iLength);
	CPro1.GlbProCreate(uszPacket, GLB_PACKET);

	/* parse */
	CPro2.GlbProParse(uszPacket, GLB_PRO_HEAD + CPro1.GlbProGetLength());
	CPro2.GlbProDisplay();
	if (NULL != (pstKv = (GLBPROKV_S*)GlbMalloc(sizeof(GLBPROKV_S) * CPro2.GlbProGetTotal()))) {
		GlbProKvInput(pstKv, CPro2.GlbProGetTotal(), CPro2.GlbProGetBody(), CPro2.GlbProGetLength());
		GlbProKvDisplay(pstKv, CPro2.GlbProGetTotal());
		GlbFree((void**)&pstKv);
	}

	return GLB_SUCCESS;
}
