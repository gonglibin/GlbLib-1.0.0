/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbPool.h			*/
/********************************************************/

#ifndef _GLBPOOL_H
#define	_GLBPOOL_H

#include "GlbMember.h"

namespace GlbCls
{

class CGlbPool
{
public:
	CGlbPool();
	CGlbPool(USHORT usTotal);
	virtual ~CGlbPool();

	void GlbPoolJoin();
	int GlbPoolCreate();
	int GlbPoolAttrInit();
	void GlbPoolDestroy();
	USHORT GlbPoolTotalGet();
	void GlbPoolDistribute(void* pPara);
	void GlbPoolTotalSet(USHORT usTotal);
	void GlbPoolRoutineSet(GLB_ROUTINE pRoutine);

private:
	USHORT m_usTotal;
	USHORT m_usCursor;
	CGlbMember* m_pCMember;
	GLB_ROUTINE m_pRoutine;
	pthread_attr_t m_stAttr;

};

} /* GlbCls */

#endif /* _GLBPOOL_H */
