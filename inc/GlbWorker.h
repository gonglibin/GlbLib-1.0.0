/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbWorker.h			*/
/********************************************************/

#ifndef _GLBWORKER_H
#define	_GLBWORKER_H

#include "GlbThread.h"
#include "GlbMtLock.h"

namespace GlbCls
{

class CGlbWorker : public CGlbThread, public CGlbMtLock
{
public:
	CGlbWorker();
	virtual ~CGlbWorker();

	void* GlbWorkerPara();
	void GlbWorkerRecycle();
	int GlbWorkerAppoint(void* pPara);

	int GlbWorkerCreate(void* pPara);

protected:
	void* m_pPara;

private:

};

} /* GlbCls */

#endif /* _GLBWORKER_H */
