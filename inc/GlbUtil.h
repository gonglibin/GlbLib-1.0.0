/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbUtl.h			*/
/********************************************************/

#ifndef _GLBUTL_H
#define	_GLBUTL_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "GlbGlobal.h"

#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/resource.h>

#define	GLB_DAYTIME			32

void GlbLocalDay(char* pszDay);
void GlbLocalTime(char* pszTime);
void GlbLocalDayTime(char* pszTime);

void GlbFree(void** ppFree);
void* GlbMalloc(ULONG ulSize);
void GlbReset(void* pReset, ULONG ulSize);

void GlbFork();
char* GlbLocalPath(char* pszEnv);
void GlbPacketDisplay(UCHAR* puszPacket, int iLength);

UCHAR* GlbBaseEncode(UCHAR *puszSrc, int iSrc, int* piDst);
UCHAR* GlbBaseDecode(UCHAR *puszSrc, int iSrc, int* piDst);

void GlbIpcShmKernel(ULONG ulMax, ULONG ulAll);
void GlbIpcMsgKernel(ULONG ulMax, ULONG ulMnb, ULONG ulMni);

#define	GLB_CDCLS			0
#define	GLB_CDOPN			RLIM_INFINITY

int GlbMaxFileGet();
int GlbCoreDump(int iType);
int GetMaxFileSet(int iNum);

#define	GLB_KENCD			0x00
#define	GLB_KDECD			0x01
#define	GLB_KSIZE			0x40

void GlbKeyCode(int iFlag, char* pszKey, char* pszSrc, char* pszDst);

char* GlbUrlEncode(char* pszSrc);
char* GlbUrlDecode(char* pszSrc);
char GlbUrlCodeHexFrom(char cKey);
char GlbUrlCodeHexInto(char cKey);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _GLBUTL_H */
