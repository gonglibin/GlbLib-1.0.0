/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbTmap.h			*/
/********************************************************/

#ifndef _GLBTMAP_H
#define	_GLBTMAP_H

#include "GlbMap.h"
#include "GlbRwLock.h"

namespace GlbCls
{

template<typename K, typename V, typename C>
class CGlbTmap : public CGlbMap<K, V, C>
{
public:
	CGlbTmap() {};
	virtual ~CGlbTmap() {};

	void GlbTmapDisplay();
	void GlbTmapDestroy();
	int GlbTmapCreateLock();
	void GlbTmapDeleteLock();

	inline V* GlbTmapSelect(K);
	inline void GlbTmapDelete(K);
	inline bool GlbTmapInsert(K, V&);
	inline bool GlbTmapUpdate(K, V&);
	inline bool GlbTmapSelect(K, V&, int);

private:
	int GlbTmapUnlock();
	int GlbTmapReadLock();
	int GlbTmapWriteLock();

	CGlbRwLock m_CLock;

};

template<typename K, typename V, typename C>
int CGlbTmap<K, V, C>::GlbTmapCreateLock()
{
	if (NULL == m_CLock.GlbLockGetLock()) {
		if (GLB_SUCCESS == m_CLock.GlbLockMalloc(NULL)) {
			if (GLB_SUCCESS != m_CLock.GlbLockCreate(false)) {
				return GLB_FAILURE;
			}
		}
		else {
			return GLB_FAILURE;
		}
	}

	return GLB_SUCCESS;
}

template<typename K, typename V, typename C>
void CGlbTmap<K, V, C>::GlbTmapDeleteLock()
{
	if (NULL != m_CLock.GlbLockGetLock()) {
		m_CLock.GlbLockDelete();
		m_CLock.GlbLockFree();
	}

	return;
}

template<typename K, typename V, typename C>
int CGlbTmap<K, V, C>::GlbTmapUnlock()
{
	return m_CLock.GlbLockUnlock();
}

template<typename K, typename V, typename C>
int CGlbTmap<K, V, C>::GlbTmapReadLock()
{

	return m_CLock.GlbLockLock(GLB_RW_RDLK);
}

template<typename K, typename V, typename C>
int CGlbTmap<K, V, C>::GlbTmapWriteLock()
{
	return m_CLock.GlbLockLock(GLB_RW_WRLK);
}

template<typename K, typename V, typename C>
void CGlbTmap<K, V, C>::GlbTmapDestroy()
{
	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapWriteLock();
		CGlbMap<K, V, C>::GlbMapDestroy();
		GlbTmapUnlock();
	}

	return;
}

template<typename K, typename V, typename C>
void CGlbTmap<K, V, C>::GlbTmapDisplay()
{
	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapReadLock();
		CGlbMap<K, V, C>::GlbMapDisplay();
		GlbTmapUnlock();
	}

	return;
}

template<typename K, typename V, typename C>
inline void CGlbTmap<K, V, C>::GlbTmapDelete(K Key)
{
	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapWriteLock();
		CGlbMap<K, V, C>::GlbMapDelete(Key);
		GlbTmapUnlock();
	}

	return;
}

template<typename K, typename V, typename C>
inline bool CGlbTmap<K, V, C>::GlbTmapInsert(K Key, V& Value)
{
	bool bReturn = false;

	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapWriteLock();
		bReturn = CGlbMap<K, V, C>::GlbMapInsert(Key, Value);
		GlbTmapUnlock();
	}

	return bReturn;
}

template<typename K, typename V, typename C>
inline bool CGlbTmap<K, V, C>::GlbTmapUpdate(K Key, V& Value)
{
	bool bReturn = false;

	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapWriteLock();
		CGlbMap<K, V, C>::GlbMapDelete(Key);
		bReturn = CGlbMap<K, V, C>::GlbMapInsert(Key, Value);
		GlbTmapUnlock();
	}

	return bReturn;
}

template<typename K, typename V, typename C>
inline bool CGlbTmap<K, V, C>::GlbTmapSelect(K Key, V& Value, int iSize)
{
	bool bReturn = false;

	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapReadLock();
		bReturn = CGlbMap<K, V, C>::GlbMapSelect(Key, Value, iSize);
		GlbTmapUnlock();
	}

	return bReturn;
}

template<typename K, typename V, typename C>
inline V* CGlbTmap<K, V, C>::GlbTmapSelect(K Key)
{
	V* pReturn = NULL;

	if (NULL != m_CLock.GlbLockGetLock()) {
		GlbTmapReadLock();
		pReturn = (V*)CGlbMap<K, V, C>::GlbMapSelect(Key);
		GlbTmapUnlock();
	}

	return pReturn;
}

} /* GlbCls */

#endif /* _GLBTMAP_H */
