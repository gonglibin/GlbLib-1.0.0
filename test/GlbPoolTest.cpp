/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbPoolTest.cpp			*/
/********************************************************/

#include "GlbPool.h"
#include <unistd.h>

using namespace GlbCls;

void* GlbPoolTestCore(void* pKey)
{
	*(UINT*)pKey += 1;
	GLB_PRINT("Thread[%lu]: %d\n", pthread_self(), *(UINT*)pKey);

	return NULL;
}

int main(int argc, char* argv[])
{
	CGlbPool CPool;
	UINT uiCount = 0;
	UINT uiValue = 0;

	CPool.GlbPoolAttrInit();
	CPool.GlbPoolTotalSet(10);
	CPool.GlbPoolRoutineSet(GlbPoolTestCore);
	CPool.GlbPoolCreate();
	while (uiCount ++ < 10) {
		CPool.GlbPoolDistribute(&uiValue);
	}
	sleep(1);

	return GLB_SUCCESS;
}
