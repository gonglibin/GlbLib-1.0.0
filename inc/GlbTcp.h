/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbTcp.h			*/
/********************************************************/

#ifndef _GLBTCP_H
#define	_GLBTCP_H

#include "GlbNet.h"
#include <netinet/tcp.h>

namespace GlbNet
{

#define	GLB_LINGER			1
#define	GLB_LISTEN			20

#define	GLB_SHUT_RD			SHUT_RD
#define	GLB_SHUT_WR			SHUT_WR
#define	GLB_SHUT_RDWR			SHUT_RDWR

class CGlbTcp : public CGlbNet
{
public:
	CGlbTcp();
	virtual ~CGlbTcp();

	void GlbTcpAccept();
	int GlbTcpSetNoDelay();
	int GlbTcpInit(char* pszPort);
	void GlbTcpShutdown(int iHow);
	int GlbTcpConnect(char* pszUrl);
	int GlbTcpSetLinger(int iSecond);
	void GlbTcpSetFunction(void (*pFunction)(int));
	int GlbTcpConnect(struct sockaddr_in* pstAddr);
	int GlbTcpConnect(char* pszAddr, char* pszPort);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength);

	virtual int GlbNetSendAll(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecvAll(UCHAR* puszPacket, int iLength);

private:
	void (*m_pFunction)(int);

};

inline int CGlbTcp::GlbNetSend(UCHAR* puszPacket, int iLength)
{
	return send(CGlbNet::m_iSocket, puszPacket, iLength, 0);
}

inline int CGlbTcp::GlbNetRecv(UCHAR* puszPacket, int iLength)
{
	return recv(CGlbNet::m_iSocket, puszPacket, iLength, 0);
}

} /* GlbNet */

#endif /* _GLBTCP_H */
