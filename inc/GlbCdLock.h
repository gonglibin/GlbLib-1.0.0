/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbCdLock.h			*/
/********************************************************/

#ifndef _GLBCDLOCK_H
#define	_GLBCDLOCK_H

#include "GlbLock.h"

namespace GlbCls
{

class CGlbCdLock : public CGlbLock
{
public:
	CGlbCdLock();
	virtual ~CGlbCdLock();

	void GlbCdLockWait();
	void GlbCdLockSignal();

	virtual int GlbLockDelete();
	virtual int GlbLockMalloc(void* pLock);
	virtual int GlbLockCreate(bool bShared);

protected:
	pthread_cond_t m_stCond;

private:

};

inline void CGlbCdLock::GlbCdLockWait()
{
	pthread_cond_wait(&m_stCond, (pthread_mutex_t*)CGlbLock::m_pLock);

	return;
}

inline void CGlbCdLock::GlbCdLockSignal()
{
	pthread_cond_signal(&m_stCond);

	return;
}

} /* GlbCls */

#endif /* _GLBCDLOCK_H */
