/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbListTest.cpp			*/
/********************************************************/

#include "GlbList.h"

using namespace GlbCls;

void* GlbLtDup(void* pKey)
{
	void* pVal = NULL;

	if (NULL != (pVal = GlbMalloc(sizeof(UINT)))) {
		memcpy(pVal, pKey, sizeof(UINT));
	}

	return pVal;
}

void GlbLtFree(void* pVal)
{
	GlbFree(&pVal);

	return;
}

int GlbLtMatch(void* pKey1, void* pKey2)
{
	return memcmp(pKey1, pKey2, sizeof(UINT));
}

int main(int argc, char* argv[])
{
	CGlbList CList;
	CGlbList CBack;
	UINT uiKey = 0;
	UINT uiCount = 0;
	UINT* puiVal = NULL;
	GLBNODE_S* pstNode = NULL;
	GLBNODE_S* pstTemp = NULL;

	/* insert */
	CList.GlbListSetDup(GlbLtDup);
	CList.GlbListSetFree(GlbLtFree);
	CList.GlbListSetMatch(GlbLtMatch);
	for (; uiCount < 10; uiCount ++) {
		if (NULL != (puiVal = (UINT*)GlbLtDup(&uiKey))) {
			CList.GlbListAddTail(puiVal);
			uiKey += 1;
		}
		else {
			break;
		}
	}

	/* display */
	uiCount = 0;
	CList.GlbListIterator(GLB_HEAD);
	GLB_PRINT("=== Display CList ===\n");
	while (NULL != (pstNode = CList.GlbListNext())) {
		GLB_PRINT("Clist.Val[%04d]: %d\n", uiCount ++, *(UINT*)pstNode->m_pValue);
	}

	/* backup */
	uiCount = 0;
	CBack.GlbListDup(CList);
	CBack.GlbListIterator(GLB_HEAD);
	GLB_PRINT("=== Display CBack ===\n");
	while (NULL != (pstNode = CBack.GlbListNext())) {
		GLB_PRINT("CBack.Val[%04d]: %d\n", uiCount ++, *(UINT*)pstNode->m_pValue);
		if (6 == uiCount) {
			pstTemp = pstNode;
		}
	}

	/* delete */
	uiCount = 0;
	CBack.GlbListRewindHead();
	CBack.GlbListDelete(pstTemp);
	GLB_PRINT("=== Delete CBack ===\n");
	while (NULL != (pstNode = CBack.GlbListNext())) {
		GLB_PRINT("CBack.Val[%04d]: %d\n", uiCount ++, *(UINT*)pstNode->m_pValue);
		if (6 == uiCount) {
			pstTemp = pstNode;
		}
	}

	/* insert */
	uiKey = 555;
	uiCount = 0;
	CBack.GlbListRewindHead();
	GLB_PRINT("=== Insert CBack ===\n");
	if (NULL != (puiVal = (UINT*)GlbLtDup(&uiKey))) {
		CBack.GlbListInsert(pstTemp, puiVal, GLB_HEAD);
		while (NULL != (pstNode = CBack.GlbListNext())) {
			GLB_PRINT("CBack.Val[%04d]: %d\n", uiCount ++, *(UINT*)pstNode->m_pValue);
		}
	}

	/* search */
	uiKey = 3;
	GLB_PRINT("=== Search CBack ===\n");
	if (NULL != (pstNode = CBack.GlbListSearch(&uiKey))) {
		GLB_PRINT("Found %d\n", *(UINT*)pstNode->m_pValue);
	}
	else {
		GLB_PRINT("Not found %d\n", uiKey);
	}

	/* index */
	GLB_PRINT("=== Index CBack ===\n");
	if (NULL != (pstNode = CBack.GlbListIndex(2))) {
		GLB_PRINT("Index %d\n", *(UINT*)pstNode->m_pValue);
	}
	else {
		GLB_PRINT("Index NULL\n");
	}

	/* rotate */
	uiCount = 0;
	CBack.GlbListRotate();
	CBack.GlbListRewindHead();
	GLB_PRINT("=== Rotate CBack ===\n");
	while (NULL != (pstNode = CBack.GlbListNext())) {
		GLB_PRINT("CBack.Val[%04d]: %d\n", uiCount ++, *(UINT*)pstNode->m_pValue);
	}

	CList.GlbListRelease();
	CBack.GlbListRelease();

	return GLB_SUCCESS;
}
