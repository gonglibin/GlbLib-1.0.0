/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbQueueTest.cpp		*/
/********************************************************/

#include "GlbQueue.h"
#include "GlbThread.h"

using namespace GlbCls;

static CGlbQueue sg_CQueue;

void* GlbPutThread(void* pNULL)
{
	UINT uiPut = 0;
	UINT uiCount = 0;

	try {
		for (; uiCount < 10; uiCount ++, ++ uiPut) {
			sleep(1);
			sg_CQueue.GlbQueuePut(&uiPut, sizeof(UINT));
			sg_CQueue.GlbQueuePut((char*)"abcdefg", strlen("abcdefg"));
		}
	}
	catch(std::exception& e) {
		GLB_PRINT("%s\n", e.what());
	}

	pthread_exit(NULL);
}

void* GlbGetThread(void* pNULL)
{
	UINT uiGet = 0;
	UINT uiCount = 0;
	char szGet[8] = { 0 };

	try {
		while (uiCount ++ < 10) {
			sg_CQueue.GlbQueueGet(&uiGet, sizeof(UINT));
			GLB_PRINT("Get: %d\n", uiGet);
			sg_CQueue.GlbQueuePop(szGet, sizeof(szGet));
			GLB_PRINT("Get: %s\n", szGet);
		}
	}
	catch(std::exception& e) {
		GLB_PRINT("%s\n", e.what());
	}

	pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
	UINT uiCount = 0;
	CGlbThread CThread[2];

	CThread[0].GlbThreadRoutineSet(GlbPutThread);
	CThread[0].GlbThreadCreate(NULL);

	CThread[1].GlbThreadRoutineSet(GlbGetThread);
	CThread[1].GlbThreadCreate(NULL);

	while (uiCount < 2) {
		CThread[uiCount].GlbThreadJoin(NULL);
		uiCount += 1;
	}

	return GLB_SUCCESS;
}
