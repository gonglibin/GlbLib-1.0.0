# Author:	gong_libin
# Date:		2016_06_01

CC = g++
CFLAGS = -g -Wall -fPIC

GLB_ENV = $(shell pwd)
GLB_BIN = $(GLB_ENV)/bin
GLB_INC = $(GLB_ENV)/inc
GLB_LIB = $(GLB_ENV)/lib
GLB_SRC = $(GLB_ENV)/src

GLB_CLS = $(GLB_SRC)/GlbCls
GLB_IPC = $(GLB_SRC)/GlbIpc
GLB_NET = $(GLB_SRC)/GlbNet
GLB_OBJ = $(GLB_SRC)/GlbObj
GLB_UTL = $(GLB_SRC)/GlbUtl

GLB_GLIB = /usr/lib
GLB_GINC = /usr/include/GlbInc

export CC
export CFLAGS

export GLB_ENV
export GLB_BIN
export GLB_INC
export GLB_LIB
export GLB_SRC

export GLB_CLS
export GLB_IPC
export GLB_NET
export GLB_OBJ
export GLB_UTL

export GLB_GLIB
export GLB_GINC

.PHONY: bin lib inc cls ipc net utl obj test tool

objects: cls ipc net utl clear
install: bin lib inc obj test tool

all: bin lib inc cls ipc net utl obj test tool

bin:
	if test -d $(GLB_BIN); then true; else mkdir $(GLB_BIN); fi;

lib:
	if test -d $(GLB_LIB); then true; else mkdir $(GLB_LIB); fi;

inc:
	if test -d $(GLB_GINC); then rm -rf $(GLB_GINC); else mkdir $(GLB_GINC); cp $(GLB_INC)/* $(GLB_GINC); fi;

cls:
	$(MAKE) -C $(GLB_CLS)

ipc:
	$(MAKE) -C $(GLB_IPC)

net:
	$(MAKE) -C $(GLB_NET)

utl:
	$(MAKE) -C $(GLB_UTL)

obj:
	$(MAKE) -C $(GLB_OBJ)

test:
	$(MAKE) -C test

tool:
	$(MAKE) -C tool

clear:
	rm -rf $(GLB_CLS)
	rm -rf $(GLB_IPC)
	rm -rf $(GLB_NET)
	rm -rf $(GLB_UTL)

clean:
	$(MAKE) clean -C $(GLB_CLS)
	$(MAKE) clean -C $(GLB_IPC)
	$(MAKE) clean -C $(GLB_NET)
	$(MAKE) clean -C $(GLB_UTL)
	$(MAKE) clean -C $(GLB_OBJ)
	$(MAKE) clean -C test
	$(MAKE) clean -C tool
	rm -rf $(GLB_GINC)
	rm -rf $(GLB_BIN)
	rm -rf $(GLB_LIB)
