/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	Bloom:		GlbBloom.cpp			*/
/********************************************************/

#include "GlbBloom.h"

namespace GlbCls
{

CGlbBloom::CGlbBloom()
{
	m_ulSize = 0;
	m_puszBloom = NULL;
	m_iBloom = GLB_FAILURE;
}

CGlbBloom::~CGlbBloom()
{
}

ULONG CGlbBloom::GlbBloomGetSize()
{
	return m_ulSize;
}

UCHAR* CGlbBloom::GlbBloomGetBloom()
{
	return m_puszBloom;
}

int CGlbBloom::GlbBloomCreate(key_t iKey, ULONG ulSize)
{
	int iReturn = GLB_SUCCESS;

	if (GLB_FAILURE != (m_iBloom = shmget(iKey, ulSize, IPC_CREAT | 0777))) {
		m_ulSize = ulSize;
		if ((void*)GLB_FAILURE == (m_puszBloom = (UCHAR*)shmat(m_iBloom, NULL, 0))) {
			GLB_ERROR("%s\n", strerror(errno));
			iReturn = GLB_FAILURE;
		}
	}
	else {
		GLB_ERROR("%s\n", strerror(errno));
		iReturn = GLB_FAILURE;
	}

	return iReturn;
}

void CGlbBloom::GlbBloomReset()
{
	memset(m_puszBloom, '\0', m_ulSize);

	return;
}

void CGlbBloom::GlbBloomDelete()
{
	shmctl(m_iBloom, IPC_RMID, 0);
	m_iBloom = GLB_FAILURE;
	m_puszBloom = NULL;
	m_ulSize = 0;

	return;
}

} /* GlbCls */
