/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbKeyTest.cpp			*/
/********************************************************/

#include "GlbUtil.h"

int main(int argc, char* argv[])
{
	char szDst[GLB_BYTE128] = {0};
	char szSrc[GLB_BYTE128] = "Copyright (C) 2016 Gong Li Bin (GlbLib-1.0.0)";
	char* pszKey = (char*)"1415926535897932384626433832795028841971693993751058209749445923";

	GLB_PRINT("Source: %s\n", szSrc);

	/* encode */
	GlbKeyCode(GLB_KENCD, pszKey, szSrc, szDst);
	GLB_PRINT("Encode: %s\n", szDst);

	/* decode */
	memset(szSrc, '\0', GLB_BYTE128);
	GlbKeyCode(GLB_KDECD, pszKey, szDst, szSrc);
	GLB_PRINT("Decode: %s\n", szSrc);

	return GLB_SUCCESS;
}
