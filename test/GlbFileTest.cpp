/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbFileTest.cpp			*/
/********************************************************/

#include "GlbFile.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbFile CFile;
	UCHAR uszGet[GLB_PACKET] = { 0 };
	UCHAR uszPut[GLB_PACKET] = "abc";
	char szFile[GLB_PACKET] = "GlbFileTest.log";

	if (GLB_SUCCESS == CFile.GlbFileCreate(szFile)) {
		CFile.GlbFileTruncate(szFile, 1048576);
		if (GLB_SUCCESS == CFile.GlbFileOpen(szFile, GLB_O_RDWR)) {
			if (GLB_SUCCESS == CFile.GlbFileMmap(GLB_P_READ | GLB_P_WRITE, GLB_M_SHARED, 0)) {
				GLB_PRINT("File: 0x%08x\n", *(UINT*)CFile.GlbFileGetFile());
				CFile.GlbFileMmapGet(0, uszGet, GLB_PACKET - 1, false);
				GLB_PRINT("Get: %s\n", uszGet);
				CFile.GlbFileMmapPut(3, uszPut, strlen((char*)uszPut), false);
				GLB_PRINT("Put: %s\n", uszPut);
				CFile.GlbFileMunmap(0);
			}
			CFile.GlbFileClose();
		}

		CFile.GlbFileDelete(szFile);
	}

	return GLB_SUCCESS;
}
