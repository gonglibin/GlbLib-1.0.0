/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbAcbm.h			*/
/********************************************************/

#ifndef	_GLBACBM_H
#define	_GLBACBM_H

#include "GlbUtil.h"
#include <ctype.h>

namespace GlbCls
{

#define	GLB_ACBM				256

typedef struct tagGlbAcbmData
{
	int m_iLength;
	UCHAR m_uszData[GLB_BYTE256];
}GLBACBMDATA_S;

typedef struct tagGlbAcbmMatch
{
	int m_iIndex;
	ULONG m_ulOffset;
}GLBACBMMATCH_S;

typedef struct tagGlbAcbmNode
{
	int m_iLabel;
	int m_iDepth;

	UCHAR m_ucVal;
	int m_iGSShift;
	int m_iBCShift;

	int m_iChild;
	UCHAR m_ucChild;
	struct tagGlbAcbmNode* m_pstParent;
	struct tagGlbAcbmNode* m_pstChild[GLB_ACBM];
}GLBACBMNODE_S;

typedef struct tagGlbAcbmTree
{
	int m_iPtnCount;
	int m_iMaxDepth;
	int m_iMinPtnSize;
	int m_iBCShift[GLB_ACBM];
	GLBACBMNODE_S* m_pstRoot;
	GLBACBMDATA_S* m_pstPtnList;
}GLBACBMTREE_S;

class CGlbAcbm
{
public:
	CGlbAcbm();
	virtual ~CGlbAcbm();

	void GlbAcbmShift();
	void GlbAcbmClean();
	void GlbAcbmGSShift();
	void GlbAcbmBCShift();
	void GlbAcbmGSShiftInit();
	void GlbAcbmTreeClean(GLBACBMNODE_S* pstRoot);
	int GlbAcbmTreeBuild(GLBACBMDATA_S* pstData, int iNumber);
	int GlbAcbmGSShiftInitCore(GLBACBMNODE_S* pstRoot, int iShift);
	int GlbAcbmSetGSShift1(UCHAR* puszChar, int iDepth, int iShift);
	GLBACBMTREE_S* GlbAcbmTreeInit(GLBACBMDATA_S* pstData, int iNumber);
	int GlbAcbmSetGSShift2(UCHAR* puszOne, int iOne, UCHAR* puszTwo, int iTwo);
	int GlbAcbmSearch(UCHAR* puszText, int iText, GLBACBMMATCH_S stMatch[], int iMax);
	void GlbAcbmDisplayKey(GLBACBMDATA_S* pstData, int iNumber);
	void GlbAcbmDisplayMatch(GLBACBMDATA_S* pstData, GLBACBMMATCH_S stMatch[], int iMatch);

private:
	GLBACBMTREE_S* m_pstTree;

};

} /* GlbCls */

#endif /* _GLBACBM_H */
