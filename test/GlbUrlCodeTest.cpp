/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbUrlCodeTest.cpp		*/
/********************************************************/

#include "GlbUtil.h"

int main(int argc, char* argv[])
{
	char* pszDst = NULL;
	char* pszTmp = NULL;
	char szSrc[] = "updateversion={\"bdapplocatesetting\":0,\"format\":\"json\"}";

	if (NULL != (pszTmp = GlbUrlEncode(szSrc))) {
		GLB_PRINT("Encode:\n");
		GLB_PRINT("Src[%d]: %s\n", strlen(szSrc), szSrc);
		GLB_PRINT("Dst[%d]: %s\n", strlen(pszTmp), pszTmp);
		if (NULL != (pszDst = GlbUrlDecode(pszTmp))) {
			GLB_PRINT("Decode:\n");
			GLB_PRINT("Src[%d]: %s\n", strlen(pszTmp), pszTmp);
			GLB_PRINT("Dst[%d]: %s\n", strlen(pszDst), pszDst);
			GlbFree((void**)&pszDst);
		}
		GlbFree((void**)&pszTmp);
	}

	return GLB_SUCCESS;
}
