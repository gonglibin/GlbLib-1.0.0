/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbNet.h			*/
/********************************************************/

#ifndef _GLBNET_H
#define	_GLBNET_H

#include "GlbGlobal.h"
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

namespace GlbNet
{

#define	GLB_URL					22

#define	GLB_SEC					1
#define	GLB_USEC					500000

#define	GLB_TIMEOUT				3

#define	GLB_MACIPUDP				42
#define	GLB_MACIPTCP				54
#define	GLB_ETHERNET				1500

#define	GLB_SOCK_MAX				50000
#define	GLB_SOCK_BUF				1048576

#define	GLB_AF_INET				AF_INET
#define	GLB_AF_UNIX				AF_UNIX

#define	GLB_SOCK_DGRAM				SOCK_DGRAM
#define	GLB_SOCK_STREAM				SOCK_STREAM

class CGlbNet
{
public:
	CGlbNet();
	virtual ~CGlbNet();

	void GlbNetClose();
	int GlbNetSetBlock();
	int GlbNetGetSocket();
	int GlbNetSetNonBlock();
	int GlbNetSetReuseAddr();
	int GlbNetBind(char* pszUrl);
	int GlbNetBind(USHORT usPort);
	int GlbNetLimit(UINT uiTotal);
	char* GlbNetUrl(char* pszUrl);
	bool GlbNetOkUrl(char* pszUrl);
	bool GlbNetOkHost(char* pszHost);
	bool GlbNetOkPort(char* pszPort);
	bool GlbNetOkPort(USHORT usPort);
	int GlbNetSetSndBuf(UINT uiSize);
	int GlbNetSetRcvBuf(UINT uiSize);
	void GlbNetSetSocket(int iSocket);
	int GlbNetOpen(int iDom, int iType);
	int GlbNetBind(void* pAddr, int iAddr);
	void GlbNetSetTimeOut(int iSec, int iUsec);
	int GlbNetBind(struct sockaddr_in* pstAddr);
	int GlbNetBind(struct sockaddr_un* pstAddr);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength) { return GLB_SUCCESS; }
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength) { return GLB_SUCCESS; }
	virtual int GlbNetSendAll(UCHAR* puszPacket, int iLength) { return GLB_SUCCESS; }
	virtual int GlbNetRecvAll(UCHAR* puszPacket, int iLength) { return GLB_SUCCESS; }

	virtual int GlbNetInitSend(char* pszUrl) { return GLB_SUCCESS; }
	virtual int GlbNetInitRecv(char* pszUrl) { return GLB_SUCCESS; }
	virtual int GlbNetInitSend(struct sockaddr_in* pstAddr) { return GLB_SUCCESS; }
	virtual int GlbNetInitRecv(struct sockaddr_in* pstAddr) { return GLB_SUCCESS; }
	virtual int GlbNetInitSend(struct sockaddr_un* pstAddr) { return GLB_SUCCESS; }
	virtual int GlbNetInitRecv(struct sockaddr_un* pstAddr) { return GLB_SUCCESS; }
	virtual int GlbNetInitSend(char* pszAddress, char* pszPort) { return GLB_SUCCESS; }
	virtual int GlbNetInitRecv(char* pszAddress, char* pszPort) { return GLB_SUCCESS; }

protected:
	int m_iSocket;

private:

};

} /* GlbNet */

#endif /* _GLBNET_H */
