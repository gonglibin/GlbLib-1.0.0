/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbLog.h			*/
/********************************************************/

#ifndef	_GLBLOG_H
#define	_GLBLOG_H

#include "GlbGlobal.h"

namespace GlbCls
{

#define	GLB_LOG_READ			(char*)"r"
#define	GLB_LOG_WRITE			(char*)"w"
#define	GLB_LOG_APPEND			(char*)"a"

class CGlbLog
{
public:
	CGlbLog();
	virtual ~CGlbLog();

	void GlbLogFlush();
	void GlbLogClose();
	FILE* GlbLogGetLog();
	int GlbLogOpen(char* pszLog, char* pszMode);
	int GlbLogRead(char* pszLine, UINT uiLength);
	int GlbLogWrite(char* pszLine, UINT uiLength);
	int GlbLogFputs(char* pszLine, UINT uiLength);
	char* GlbLogFgets(char* pszLine, UINT uiLength);

protected:
	FILE* m_pstLog;

private:

};

inline int CGlbLog::GlbLogRead(char* pszLine, UINT uiLength)
{
	return fread((void*)pszLine, sizeof(char), uiLength, m_pstLog);
}

inline int CGlbLog::GlbLogWrite(char* pszLine, UINT uiLength)
{
	return fwrite((void*)pszLine, sizeof(char), uiLength, m_pstLog);
}

inline int CGlbLog::GlbLogFputs(char* pszLine, UINT uiLength)
{
	return fputs(pszLine, m_pstLog);
}

inline char* CGlbLog::GlbLogFgets(char* pszLine, UINT uiLength)
{
	return fgets(pszLine, uiLength, m_pstLog);
}

} /* GlbCls */

#endif /* _GLBLOG_H */
