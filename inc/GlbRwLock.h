/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbRwLock.h			*/
/********************************************************/

#ifndef _GLBRWLOCK_H
#define	_GLBRWLOCK_H

#include "GlbLock.h"

namespace GlbCls
{

#define	GLB_RW_RDLK			0x01
#define	GLB_RW_WRLK			0x02

class CGlbRwLock : public CGlbLock
{
public:
	CGlbRwLock();
	virtual ~CGlbRwLock();

	int GlbLockTryLock(UCHAR ucType);

	virtual int GlbLockDelete();
	virtual int GlbLockUnlock();
	virtual int GlbLockLock(UCHAR ucType);
	virtual int GlbLockCreate(bool bShared);
	virtual int GlbLockMalloc(void* pRwLock);

private:
	int GlbRwLockReadLock();
	int GlbRwLockWriteLock();
	int GlbRwLockTryReadLock();
	int GlbRwLockTryWriteLock();

};

inline int CGlbRwLock::GlbLockUnlock()
{
	return pthread_rwlock_unlock((pthread_rwlock_t*)CGlbLock::m_pLock);
}

inline int CGlbRwLock::GlbRwLockReadLock()
{
	return pthread_rwlock_rdlock((pthread_rwlock_t*)CGlbLock::m_pLock);
}

inline int CGlbRwLock::GlbRwLockWriteLock()
{
	return pthread_rwlock_wrlock((pthread_rwlock_t*)CGlbLock::m_pLock);
}

inline int CGlbRwLock::GlbRwLockTryReadLock()
{
	return pthread_rwlock_tryrdlock((pthread_rwlock_t*)CGlbLock::m_pLock);
}

inline int CGlbRwLock::GlbRwLockTryWriteLock()
{
	return pthread_rwlock_trywrlock((pthread_rwlock_t*)CGlbLock::m_pLock);
}

} /* GlbCls */

#endif /* _GLBRWLOCK_H */
