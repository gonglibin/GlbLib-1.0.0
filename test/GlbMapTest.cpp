/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMapTest.cpp			*/
/********************************************************/

#include "GlbMap.h"
#include "GlbTime.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbTime CTime;
	ULONG ulKey = 0;
	ULONG ulValue = 0;
	CGlbMap<ULONG, ULONG, GLBMAPDIGIT_O> CTap;

	CTime.GlbTimeOn();
	for (; ulKey < 1000000; ulKey ++) {
		CTap.GlbMapInsert(ulKey, ulValue);
		ulValue += 1;
	}
	CTime.GlbTimeOff();
	CTime.GlbTimeDisplay();

	//CTap.GlbMapDisplay();

	ulKey = 999;
	ulValue = 0;
	CTime.GlbTimeOn();
	CTap.GlbMapSelect(ulKey, ulValue, sizeof(ULONG));
	CTime.GlbTimeOff();
	CTime.GlbTimeDisplay();
	GLB_PRINT("Select { Key: %ld Value: %ld }\n", ulKey, ulValue);

	return GLB_SUCCESS;
}
