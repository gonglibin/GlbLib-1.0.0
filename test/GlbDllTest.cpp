/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDllTest.cpp			*/
/********************************************************/

#include "GlbDll.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbDll CDll;
	void* pGlbLocalDayTime = NULL;
	char szTime[GLB_BYTE32] = { 0 };
	char* pszFile = (char*)"/usr/lib/libglbglobal.so";

	if (GLB_SUCCESS == CDll.GlbDllOpen(pszFile, GLB_RTLD_LAZY)) {
		if (NULL != (pGlbLocalDayTime = CDll.GlbDllFunGet((char*)"GlbLocalDayTime"))) {
			(*(void (*)(char*))pGlbLocalDayTime)(szTime);
			GLB_PRINT("Time: %s\n", szTime);
		}
		else {
			GLB_ERROR("%s", CDll.GlbDllError());
		}
		CDll.GlbDllClose();
	}
	else {
		GLB_ERROR("%s", CDll.GlbDllError());
	}

	return GLB_SUCCESS;
}
