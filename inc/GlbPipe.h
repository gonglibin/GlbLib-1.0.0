/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbPipe.h			*/
/********************************************************/

#ifndef	_GLBPIPE_H
#define	_GLBPIPE_H

#include "GlbGlobal.h"

namespace GlbCls
{

#define	GLB_PIPE				102400

class CGlbPipe
{
public:
	CGlbPipe();
	virtual ~CGlbPipe();

	void GlbPipeClose();
	void GlbPipeResultFree();
	char* GlbPipeResultGet();
	ULONG GlbPipeRead(ULONG ulSize);
	void GlbPipeResultReset(ULONG ulSize);
	int GlbPipeResultMalloc(ULONG ulSize);
	int GlbPipeOpen(char* pszGlbd, char* pszType);

protected:
	char* m_pszResult;

private:
	FILE* m_pstPipe;

};

} /* GlbCls */

#endif /* _GLBPIPE_H */
