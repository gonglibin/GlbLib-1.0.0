/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbThread.h			*/
/********************************************************/

#ifndef _GLBTHREAD_H
#define	_GLBTHREAD_H

#include "GlbGlobal.h"
#include <pthread.h>

namespace GlbCls
{

template<typename T>
void* GlbThreadType(void* pPara)
{
	((T*)(pPara))->GlbThreadCore();

	return pPara;
}

typedef	void* (*GLB_ROUTINE)(void*);

class CGlbThread
{
public:
	CGlbThread();
	virtual ~CGlbThread();

	virtual void GlbThreadCore();
	virtual int GlbThreadDestroy();
	virtual int GlbThreadCreate(void* pPara);
	virtual int GlbThreadJoin(void** ppReturn);

	int GlbThreadAttrInit();
	int GlbThreadAttrSetDetach();
	GLB_ROUTINE GlbThreadRoutineGet();
	void GlbThreadAttrSet(void* pAttr);
	void GlbThreadRoutineSet(GLB_ROUTINE);

protected:
	pthread_t m_iID;
	GLB_ROUTINE m_pRoutine;
	pthread_attr_t m_stAttr;

private:

};

} /* GlbCls */

#endif /* _GLBTHREAD_H */
