/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbIpc.h			*/
/********************************************************/

#ifndef _GLBIPC_H
#define	_GLBIPC_H

#include "GlbGlobal.h"
#include <sys/ipc.h>

namespace GlbIpc
{

#define	GLB_IPC_NOWAIT				IPC_NOWAIT
#define	GLB_IPC_PRIVATE				IPC_PRIVATE

class CGlbIpc
{
public:
	CGlbIpc() { m_iIpc = GLB_FAILURE; }
	virtual ~CGlbIpc() { m_iIpc = GLB_FAILURE; }

	int GlbIpcGetIpc() { return m_iIpc; }
	void GlbIpcPutIpc(int iIpc) { m_iIpc = iIpc; }
	key_t GlbIpcKey(char* pszPath, int iID) { return ftok(pszPath, iID); }

	virtual int GlbIpcDelete() { return GLB_SUCCESS; }
	virtual int GlbIpcCreate(key_t iKey) { return GLB_SUCCESS; }

	virtual void GlbIpcInsert(void* pData) { return; }
	virtual void GlbIpcDelete(void* pData) { return; }
	virtual void GlbIpcUpdate(void* pData) { return; }
	virtual void* GlbIpcSelect(void* pData) { return NULL; }

protected:
	int m_iIpc;

private:

};

} /* GlbIpc */

#endif /* _GLBIPC_H */
