/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbThreadTest.cpp		*/
/********************************************************/

#include "GlbThread.h"

using namespace GlbCls;

class CGlbChild : public CGlbThread
{
public:
	CGlbChild() {};
	virtual ~CGlbChild() {};

	virtual void GlbThreadCore() {
		GLB_PRINT("Thread[%lu]: hello world\n", pthread_self());
		return;
	}

protected:

private:

};

int main(int argc, char* argv[])
{
	CGlbChild CChild;

	CChild.GlbThreadAttrInit();
	CChild.GlbThreadRoutineSet(GlbThreadType<CGlbChild>);
	CChild.GlbThreadCreate(&CChild);
	CChild.GlbThreadJoin(NULL);

	return GLB_SUCCESS;
}
