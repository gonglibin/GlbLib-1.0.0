/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbHashTest.cpp			*/
/********************************************************/

#include "GlbSet.h"
#include "GlbShm.h"
#include "GlbHash.h"
#include "GlbQueue.h"

using namespace GlbCls;
using namespace GlbIpc;

typedef struct tagGlbHashTest
{
	int m_iKey;
	CGlbSet* m_pCSet;
}CTHASHTEST_S;

int main(int argc, char* argv[])
{
	CGlbHash CHash;
	UINT uiIndex = 0;
	UINT uiValue = 1;
	UINT uiTotal = 9;
	void* pCur = NULL;
	UINT* puiSet = NULL;
	void** ppBucket = NULL;
	CTHASHTEST_S stHash;
	CTHASHTEST_S* pstHash = NULL;
	CGlbQueue* pCQueue = NULL;

	CHash.GlbHashCreate(100, 97);

	if (NULL != (pCQueue = new CGlbQueue())) {
		CHash.GlbHashPutBucket(8, (void**)&pCQueue);

		memset(&stHash, '\0', sizeof(CTHASHTEST_S));
		stHash.m_iKey = 123;
		stHash.m_pCSet = new CGlbSet();
		if (GLB_SUCCESS == stHash.m_pCSet->GlbSetInit(uiTotal, sizeof(UINT))) {
			for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
				stHash.m_pCSet->GlbSetIndexPut(uiIndex, &uiValue);
				uiValue += 1;
			}
		}
		pCQueue->GlbQueuePut(&stHash, sizeof(CTHASHTEST_S));

		memset(&stHash, '\0', sizeof(CTHASHTEST_S));
		stHash.m_iKey = 456;
		stHash.m_pCSet = new CGlbSet();
		if (GLB_SUCCESS == stHash.m_pCSet->GlbSetInit(uiTotal, sizeof(UINT))) {
			for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
				stHash.m_pCSet->GlbSetIndexPut(uiIndex, &uiValue);
				uiValue += 1;
			}
		}
		pCQueue->GlbQueuePut(&stHash, sizeof(CTHASHTEST_S));

		memset(&stHash, '\0', sizeof(CTHASHTEST_S));
		stHash.m_iKey = 789;
		stHash.m_pCSet = new CGlbSet();
		if (GLB_SUCCESS == stHash.m_pCSet->GlbSetInit(uiTotal, sizeof(UINT))) {
			for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
				stHash.m_pCSet->GlbSetIndexPut(uiIndex, &uiValue);
				uiValue += 1;
			}
		}
		pCQueue->GlbQueuePut(&stHash, sizeof(CTHASHTEST_S));

		ppBucket = (void**)CHash.GlbHashGetBucket(8);
		while (NULL != (pCur = (*(CGlbQueue**)ppBucket)->GlbQueueHasNext(pCur))) {
			pstHash = (CTHASHTEST_S*)((UCHAR*)pCur + sizeof(UINT));
			GLB_PRINT("Key: %d\n", pstHash->m_iKey);
			if (NULL != (puiSet = (UINT*)pstHash->m_pCSet->GlbSetGetSet())) {
				for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
					GLB_PRINT("Index[%04d]: %d\n", uiIndex, *(puiSet + uiIndex));
				}
				pstHash->m_pCSet->GlbSetDestroy();
				delete(pstHash->m_pCSet);
			}
		}

		delete(pCQueue);
	}

	CHash.GlbHashDelete();

	return GLB_SUCCESS;
}
