/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMd5.cpp			*/
/********************************************************/

#include "GlbMd5.h"

namespace GlbCls
{

CGlbMd5::CGlbMd5()
{
}

CGlbMd5::~CGlbMd5()
{
}

void CGlbMd5::GlbMd5Init()
{
	m_uiBuf[0] = 0x67452301;
	m_uiBuf[1] = 0xefcdab89;
	m_uiBuf[2] = 0x98badcfe;
	m_uiBuf[3] = 0x10325476;

	m_uiBits[0] = 0;
	m_uiBits[1] = 0;

	return;
}

void CGlbMd5::GlbMd5Final(UCHAR digest[16])
{
	UINT uiCount = 0;;
	UCHAR *pcCur = NULL;

	uiCount = (m_uiBits[0] >> 3) & 0x3F;

	pcCur = m_ucIn + uiCount;
	*pcCur ++ = 0x80;

	uiCount = 64 - 1 - uiCount;

	if( uiCount < 8 ) {
		memset(pcCur, 0, uiCount);
		GlbMd5ByteReverse(m_ucIn, 16);
		GlbMd5Transform(m_uiBuf, (UINT*)m_ucIn);

		memset(m_ucIn, 0, 56);
	}
	else {
		memset(pcCur, 0, uiCount - 8);
	}
	GlbMd5ByteReverse(m_ucIn, 14);

	((UINT*)m_ucIn)[14] = m_uiBits[0];
	((UINT*)m_ucIn)[15] = m_uiBits[1];

	GlbMd5Transform(m_uiBuf, (UINT*)m_ucIn);
	GlbMd5ByteReverse((UCHAR*)m_uiBuf, 4);
	memcpy(digest, m_uiBuf, 16);

	return;
}

void CGlbMd5::GlbMd5Update(UCHAR* puszBuf, UINT uiLen)
{
	UINT uiValue = 0;
	UCHAR *pcCur = NULL;

	uiValue = m_uiBits[0];
	if( (m_uiBits[0] = uiValue + ((UINT)uiLen << 3)) < uiValue ) {
		m_uiBits[1] ++;
	}
	m_uiBits[1] += uiLen >> 29;

	uiValue = (uiValue >> 3) & 0x3f;

	if( uiValue ) {
		pcCur = (UCHAR*)m_ucIn + uiValue;

		uiValue = 64 - uiValue;
		if( uiLen < uiValue ) {
			memcpy(pcCur, puszBuf, uiLen);
			return;
		}
		memcpy(pcCur, puszBuf, uiValue);
		GlbMd5ByteReverse(m_ucIn, 16);
		GlbMd5Transform(m_uiBuf, (UINT*)m_ucIn);
		puszBuf += uiValue;
		uiLen -= uiValue;
	}

	while( uiLen >= 64 ) {
		memcpy(m_ucIn, puszBuf, 64);
		GlbMd5ByteReverse(m_ucIn, 16);
		GlbMd5Transform(m_uiBuf, (UINT*)m_ucIn);
		puszBuf += 64;
		uiLen -= 64;
	}

	memcpy(m_ucIn, puszBuf, uiLen);

	return;
}

void CGlbMd5::GlbMd5Run(UCHAR* puszBuf, UINT uiLen, UCHAR digest[16])
{
	GlbMd5Init();
	GlbMd5Update(puszBuf, uiLen);
	GlbMd5Final(digest);

	return;
}

void CGlbMd5::GlbMd5Transform(UINT buf[4], UINT in[16])
{
	register UINT a, b, c, d;

	a = buf[0];
	b = buf[1];
	c = buf[2];
	d = buf[3];

	GLBMD5STEP(F1, a, b, c, d, in[0] + 0xd76aa478, 7);
	GLBMD5STEP(F1, d, a, b, c, in[1] + 0xe8c7b756, 12);
	GLBMD5STEP(F1, c, d, a, b, in[2] + 0x242070db, 17);
	GLBMD5STEP(F1, b, c, d, a, in[3] + 0xc1bdceee, 22);
	GLBMD5STEP(F1, a, b, c, d, in[4] + 0xf57c0faf, 7);
	GLBMD5STEP(F1, d, a, b, c, in[5] + 0x4787c62a, 12);
	GLBMD5STEP(F1, c, d, a, b, in[6] + 0xa8304613, 17);
	GLBMD5STEP(F1, b, c, d, a, in[7] + 0xfd469501, 22);
	GLBMD5STEP(F1, a, b, c, d, in[8] + 0x698098d8, 7);
	GLBMD5STEP(F1, d, a, b, c, in[9] + 0x8b44f7af, 12);
	GLBMD5STEP(F1, c, d, a, b, in[10] + 0xffff5bb1, 17);
	GLBMD5STEP(F1, b, c, d, a, in[11] + 0x895cd7be, 22);
	GLBMD5STEP(F1, a, b, c, d, in[12] + 0x6b901122, 7);
	GLBMD5STEP(F1, d, a, b, c, in[13] + 0xfd987193, 12);
	GLBMD5STEP(F1, c, d, a, b, in[14] + 0xa679438e, 17);
	GLBMD5STEP(F1, b, c, d, a, in[15] + 0x49b40821, 22);

	GLBMD5STEP(F2, a, b, c, d, in[1] + 0xf61e2562, 5);
	GLBMD5STEP(F2, d, a, b, c, in[6] + 0xc040b340, 9);
	GLBMD5STEP(F2, c, d, a, b, in[11] + 0x265e5a51, 14);
	GLBMD5STEP(F2, b, c, d, a, in[0] + 0xe9b6c7aa, 20);
	GLBMD5STEP(F2, a, b, c, d, in[5] + 0xd62f105d, 5);
	GLBMD5STEP(F2, d, a, b, c, in[10] + 0x02441453, 9);
	GLBMD5STEP(F2, c, d, a, b, in[15] + 0xd8a1e681, 14);
	GLBMD5STEP(F2, b, c, d, a, in[4] + 0xe7d3fbc8, 20);
	GLBMD5STEP(F2, a, b, c, d, in[9] + 0x21e1cde6, 5);
	GLBMD5STEP(F2, d, a, b, c, in[14] + 0xc33707d6, 9);
	GLBMD5STEP(F2, c, d, a, b, in[3] + 0xf4d50d87, 14);
	GLBMD5STEP(F2, b, c, d, a, in[8] + 0x455a14ed, 20);
	GLBMD5STEP(F2, a, b, c, d, in[13] + 0xa9e3e905, 5);
	GLBMD5STEP(F2, d, a, b, c, in[2] + 0xfcefa3f8, 9);
	GLBMD5STEP(F2, c, d, a, b, in[7] + 0x676f02d9, 14);
	GLBMD5STEP(F2, b, c, d, a, in[12] + 0x8d2a4c8a, 20);

	GLBMD5STEP(F3, a, b, c, d, in[5] + 0xfffa3942, 4);
	GLBMD5STEP(F3, d, a, b, c, in[8] + 0x8771f681, 11);
	GLBMD5STEP(F3, c, d, a, b, in[11] + 0x6d9d6122, 16);
	GLBMD5STEP(F3, b, c, d, a, in[14] + 0xfde5380c, 23);
	GLBMD5STEP(F3, a, b, c, d, in[1] + 0xa4beea44, 4);
	GLBMD5STEP(F3, d, a, b, c, in[4] + 0x4bdecfa9, 11);
	GLBMD5STEP(F3, c, d, a, b, in[7] + 0xf6bb4b60, 16);
	GLBMD5STEP(F3, b, c, d, a, in[10] + 0xbebfbc70, 23);
	GLBMD5STEP(F3, a, b, c, d, in[13] + 0x289b7ec6, 4);
	GLBMD5STEP(F3, d, a, b, c, in[0] + 0xeaa127fa, 11);
	GLBMD5STEP(F3, c, d, a, b, in[3] + 0xd4ef3085, 16);
	GLBMD5STEP(F3, b, c, d, a, in[6] + 0x04881d05, 23);
	GLBMD5STEP(F3, a, b, c, d, in[9] + 0xd9d4d039, 4);
	GLBMD5STEP(F3, d, a, b, c, in[12] + 0xe6db99e5, 11);
	GLBMD5STEP(F3, c, d, a, b, in[15] + 0x1fa27cf8, 16);
	GLBMD5STEP(F3, b, c, d, a, in[2] + 0xc4ac5665, 23);

	GLBMD5STEP(F4, a, b, c, d, in[0] + 0xf4292244, 6);
	GLBMD5STEP(F4, d, a, b, c, in[7] + 0x432aff97, 10);
	GLBMD5STEP(F4, c, d, a, b, in[14] + 0xab9423a7, 15);
	GLBMD5STEP(F4, b, c, d, a, in[5] + 0xfc93a039, 21);
	GLBMD5STEP(F4, a, b, c, d, in[12] + 0x655b59c3, 6);
	GLBMD5STEP(F4, d, a, b, c, in[3] + 0x8f0ccc92, 10);
	GLBMD5STEP(F4, c, d, a, b, in[10] + 0xffeff47d, 15);
	GLBMD5STEP(F4, b, c, d, a, in[1] + 0x85845dd1, 21);
	GLBMD5STEP(F4, a, b, c, d, in[8] + 0x6fa87e4f, 6);
	GLBMD5STEP(F4, d, a, b, c, in[15] + 0xfe2ce6e0, 10);
	GLBMD5STEP(F4, c, d, a, b, in[6] + 0xa3014314, 15);
	GLBMD5STEP(F4, b, c, d, a, in[13] + 0x4e0811a1, 21);
	GLBMD5STEP(F4, a, b, c, d, in[4] + 0xf7537e82, 6);
	GLBMD5STEP(F4, d, a, b, c, in[11] + 0xbd3af235, 10);
	GLBMD5STEP(F4, c, d, a, b, in[2] + 0x2ad7d2bb, 15);
	GLBMD5STEP(F4, b, c, d, a, in[9] + 0xeb86d391, 21);

	buf[0] += a;
	buf[1] += b;
	buf[2] += c;
	buf[3] += d;

	return;
}

void CGlbMd5::GlbMd5ByteReverse(UCHAR* puszBuf, UINT uiLongs)
{
	UINT uiValue;
	do {
		uiValue = (UINT)((UINT)puszBuf[3] << 8 | puszBuf[2]) << 16 | ((UINT)puszBuf[1] << 8 | puszBuf[0]);
		*(UINT*)puszBuf = uiValue;
		puszBuf += 4;
	} while( --uiLongs );

	return;
}

} /* GlbCls */
