/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbHash.h			*/
/********************************************************/

#ifndef _GLBHASH_H
#define	_GLBHASH_H

#include "GlbShm.h"
#include <ctype.h>

namespace GlbIpc
{

#define	GLB_PLUS				0x3039
#define	GLB_MASK				0x7FFFFFFF
#define	GLB_BASIC			0x238F13AF
#define	GLB_OFFSET			0x41C64E6B

/*static const ULONG sg_ulBucket[] = {
  5ul,          7ul,        53ul,         97ul,         193ul,
  389ul,       769ul,       1543ul,       3079ul,       6151ul,
  12289ul,     24593ul,     49157ul,      98317ul,      196613ul,
  393241ul,    786433ul,    1572869ul,    3145739ul,    6291469ul,
  12582917ul,  25165843ul,  50331653ul,   100663319ul,  201326611ul,
  402653189ul, 805306457ul, 1610612741ul, 3221225473ul, 4294967291ul
};*/

class CGlbHash : public CGlbShm
{
public:
	CGlbHash();
	virtual ~CGlbHash();

	void GlbHashDelete();
	void* GlbHashGetBucket(ULONGLONG ullKey);
	int GlbHashCreate(key_t iKey, ULONG ulSize);
	void GlbHashPutBucket(ULONGLONG ullKey, void** ppKey);
	ULONGLONG GlbHashFunction(char* pszKey, UINT uiLength);

protected:
	ULONG m_ulHash;

private:

};

inline void* CGlbHash::GlbHashGetBucket(ULONGLONG ullKey)
{
	return (void*)((UCHAR*)m_pShm + (sizeof(void*) * (ullKey % m_ulHash)));
}

} /* GlbIpc */

#endif /* _GLBHASH_H */
