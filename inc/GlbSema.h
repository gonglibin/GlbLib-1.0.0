/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbSema.h			*/
/********************************************************/

#ifndef _GLBSEMA_H
#define	_GLBSEMA_H

#include "GlbGlobal.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

namespace GlbCls
{

#define	GLB_FLAGS		(O_CREAT | O_EXCL)
#define	GLB_PERMS		(mode_t)(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)

class CGlbSema
{
public:
	CGlbSema();
	virtual ~CGlbSema();

	int GlbSemaPost();
	int GlbSemaWait();
	int GlbSemaTryWait();
	void GlbSemaDestroy();
	int GlbSemaGetValue(int& riValue);
	int GlbSemaInit(int iShare, UINT uiVal);

	void GlbSemaClose();
	void GlbSemaUnlink(char* pszName);
	int GlbSemaOpen(char* pszName, int iVal);

protected:
	sem_t* m_pstSema;

private:

};

} /* GlbCls */

#endif /* _GLBSEMA_H */
