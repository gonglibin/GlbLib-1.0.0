/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMsgTest.cpp			*/
/********************************************************/

#include "GlbMsg.h"
#include "GlbUtil.h"

using namespace GlbIpc;

typedef struct tagGlbMsg
{
	ULONG m_ulID;
	char m_szMsg[GLB_PACKET];
}CTMSG_S;

int main(int argc, char* argv[])
{
	CGlbMsg CTsg;
	CTMSG_S stMsg;
	UCHAR ucMsg = 'A';
	ULONG ulCount = 1;

	GlbIpcMsgKernel(81920, 163840, 16);
	if (GLB_SUCCESS == CTsg.GlbIpcCreate(CTsg.GlbIpcKey((char*)"/root", 'M'))) {
		for (ulCount = 1; ulCount < 10; ulCount ++) {
			memset(&stMsg, '\0', sizeof(CTMSG_S));
			stMsg.m_ulID = ulCount;
			sprintf(stMsg.m_szMsg, "This is message[%c].", ucMsg ++);
			CTsg.GlbMsgPut((void*)&stMsg, sizeof(CTMSG_S), 0);
		}

		ulCount = 0;
		memset(&stMsg, '\0', sizeof(CTMSG_S));
		for (ulCount = 1; ulCount < 10; ulCount ++) {
			if (CTsg.GlbMsgGet(&stMsg, sizeof(CTMSG_S), 0) > 0) {
			//while (CTsg.GlbMsgGet(&stMsg, sizeof(CTMSG_S)) > 0) {
				GLB_PRINT("Count[%03ld]: ID: %ld -> Msg: %s\n", ulCount, stMsg.m_ulID, stMsg.m_szMsg);
				memset(&stMsg, '\0', sizeof(CTMSG_S));
			}
		}

		CTsg.GlbIpcDelete();
	}

	return GLB_SUCCESS;
}
