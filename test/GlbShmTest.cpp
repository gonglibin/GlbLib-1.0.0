/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbShmTest.cpp			*/
/********************************************************/

#include "GlbSet.h"
#include "GlbShm.h"

using namespace GlbCls;
using namespace GlbIpc;

int GlbShmTestCompare(const void* m, const void* n)
{
	return *(UINT*)n - *(UINT*)m;
}

int main(int argc, char* argv[])
{
	CGlbSet CSet;
	CGlbShm CShm;
	UINT uiIndex = 0;
	UINT uiValue = 1;
	UINT uiTotal = 100;
	UINT* puiSet = NULL;

	if (GLB_SUCCESS == CShm.GlbIpcCreate(CShm.GlbIpcKey((char*)"/root", 'T'), uiTotal * sizeof(UINT))) {
		CShm.GlbShmLock();
		CShm.GlbShmReset(uiTotal * sizeof(UINT));

		CSet.GlbSetPutSize(sizeof(UINT));
		CSet.GlbSetPutSet(CShm.GlbShmGetShm());

		/* insert data */
		for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
			CSet.GlbSetIndexPut(uiIndex, &uiValue);
			uiValue += 1;
		}

		CSet.GlbSetCompare(GlbShmTestCompare);
		CSet.GlbSetSort();

		/* display data */
		if (NULL != (puiSet = (UINT*)CSet.GlbSetGetSet())) {
			for (uiIndex = 0; uiIndex < uiTotal; uiIndex ++) {
				GLB_PRINT("Index[%04d]: %d\n", uiIndex, *(puiSet + uiIndex));
			}
		}
		CShm.GlbIpcDelete();
	}

	return GLB_SUCCESS;
}
