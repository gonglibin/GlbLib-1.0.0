/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbWorker.cpp			*/
/********************************************************/

#include "GlbWorker.h"

namespace GlbCls
{

CGlbWorker::CGlbWorker()
{
	m_pPara = NULL;
}

CGlbWorker::~CGlbWorker()
{
	m_pPara = NULL;
}

void* CGlbWorker::GlbWorkerPara()
{
	return m_pPara;
}

void CGlbWorker::GlbWorkerRecycle()
{
	m_pPara = NULL;

	return;
}

int CGlbWorker::GlbWorkerAppoint(void* pPara)
{
	if (NULL == m_pPara) {
		m_pPara = pPara;
		GlbLockUnlock();
	}
	else {
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbWorker::GlbWorkerCreate(void* pPara)
{
	int iReturn = GLB_SUCCESS;

	if (GLB_SUCCESS == GlbLockCreate()) {
		iReturn = GlbThreadCreate(pPara);
	}

	return iReturn;
}

} /* GlbCls */
