/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbBloomTest.cpp		*/
/********************************************************/

#include "GlbBloom.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbBloom CBloom;
	UCHAR uszMD5[] = {0x5f, 0x0d, 0x67, 0x6c, 0xec, 0x6b, 0xaa, 0xf0, 0x14, 0x77, 0xe4, 0x17, 0xc4, 0x39, 0x4f, 0x1d};

	if (GLB_SUCCESS == CBloom.GlbBloomCreate(ftok((char*)"/tmp", 88), 128)) {
		CBloom.GlbBloomUpdate(*(ULONGLONG*)uszMD5 ^ *(ULONGLONG*)(uszMD5 + sizeof(uszMD5) / 2));
		GLB_PRINT("Check: %d\n", CBloom.GlbBloomCheck(*(ULONGLONG*)uszMD5 ^ *(ULONGLONG*)(uszMD5 + sizeof(uszMD5) / 2)));
		CBloom.GlbBloomDelete();
	}

	return GLB_SUCCESS;
}
