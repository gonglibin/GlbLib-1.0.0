/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMtLock.cpp			*/
/********************************************************/

#include "GlbMtLock.h"

namespace GlbCls
{

CGlbMtLock::CGlbMtLock()
{
}

CGlbMtLock::~CGlbMtLock()
{
}

int CGlbMtLock::GlbLockCreate()
{
	int iReturn = GLB_SUCCESS;

	if (NULL == CGlbLock::m_pLock) {
		iReturn = GlbLockMalloc(NULL);
	}

	if (GLB_SUCCESS == iReturn) {
		iReturn = pthread_mutex_init((pthread_mutex_t*)CGlbLock::m_pLock, NULL);
	}

	return iReturn;
}

int CGlbMtLock::GlbLockMalloc(void* pLock)
{
	if (NULL == pLock) {
		CGlbLock::m_pLock = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
		if (NULL == CGlbLock::m_pLock) {
			return GLB_FAILURE;
		}
	}
	else {
		CGlbLock::m_pLock = (pthread_mutex_t*)pLock;
	}

	return GLB_SUCCESS;
}

int CGlbMtLock::GlbLockCreate(bool bShared)
{
	pthread_mutexattr_t stAttr;

	if (NULL != CGlbLock::m_pLock) {
		if (GLB_SUCCESS !=  pthread_mutexattr_init(&stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}

		if (true == bShared) {
			if (GLB_SUCCESS != pthread_mutexattr_setpshared(&stAttr, PTHREAD_PROCESS_SHARED)) {
				GLB_ERROR("%s\n", strerror(errno));
				return GLB_FAILURE;
			}
		}

		if (GLB_SUCCESS != pthread_mutex_init((pthread_mutex_t*)CGlbLock::m_pLock, &stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}

		if (GLB_SUCCESS != pthread_mutexattr_destroy(&stAttr)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}
	}
	else {
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbMtLock::GlbLockDelete()
{
	if (NULL != CGlbLock::m_pLock) {
		if (GLB_SUCCESS != pthread_mutex_destroy((pthread_mutex_t*)CGlbLock::m_pLock)) {
			GLB_ERROR("%s\n", strerror(errno));
			return GLB_FAILURE;
		}
	}

	return GLB_SUCCESS;
}

} /* GlbCls */
