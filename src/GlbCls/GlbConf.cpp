/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbConf.cpp			*/
/********************************************************/

#include "GlbConf.h"

namespace GlbCls
{

CGlbConf::CGlbConf()
{
	m_pstFile = NULL;
}

CGlbConf::~CGlbConf()
{
	m_pstFile = NULL;
}

void CGlbConf::GlbConfClose()
{
	if (NULL != m_pstFile) {
		fclose(m_pstFile);
		m_pstFile = NULL;
	}

	return;
}

int CGlbConf::GlbConfOpen(char* pszFile)
{
	if (NULL == (m_pstFile = fopen(pszFile, "r"))) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_EOPN;
	}

	return GLB_SUCCESS;
}

int CGlbConf::GlbConfGetLine(char* pszLine, int iSize)
{
	int iReturn = GLB_SUCCESS;

	if (NULL != m_pstFile) {
		if (NULL == fgets(pszLine, iSize, m_pstFile)) {
			iReturn = GLB_EOTH;
		}
	}
	else {
		iReturn = GLB_EOPN;
	}

	return iReturn;
}

int CGlbConf::GlbConfGetSec(char* pszSec)
{
	int iReturn = GLB_ESEC;
	char szLine[GLB_PACKET] = { 0 };

	if (NULL != m_pstFile) {
		rewind(m_pstFile);
		while (GLB_SUCCESS == GlbConfGetLine(szLine, GLB_PACKET - 1)) {
			if (true != GlbConfIsValid(szLine)) continue;
			if (true == GlbConfIsSection(szLine)) {
				if (GLB_SUCCESS == strncasecmp(&szLine[1], pszSec, strlen(pszSec))) {
					iReturn = GLB_SUCCESS;
					break;
				}
			}
			memset(szLine, '\0', GLB_PACKET);
		}
	}
	else {
		iReturn = GLB_EOPN;
	}

	return iReturn;
}

int CGlbConf::GlbConfGetKey(char* pszSec, char* pszKey)
{
	bool bSec = false;
	int iReturn = GLB_ESEC;
	char szLine[GLB_PACKET] = { 0 };

	if (NULL != m_pstFile) {
		rewind(m_pstFile);
		while (GLB_SUCCESS == GlbConfGetLine(szLine, GLB_PACKET - 1)) {
			if (true != GlbConfIsValid(szLine)) continue;
			if (true == GlbConfIsSection(szLine)) {
				if (false == bSec) {
					if (GLB_SUCCESS == strncasecmp(&szLine[1], pszSec, strlen(pszSec))) {
						iReturn = GLB_EKEY;
						bSec = true;
					}
				}
				else {
					bSec = false;
				}
			}
			else {
				if (true == bSec) {
					if (GLB_SUCCESS == strncasecmp(szLine, pszKey, strlen(pszKey))) {
						iReturn = GLB_SUCCESS;
						break;
					}
				}
			}
			memset(szLine, '\0', GLB_PACKET);
		}
	}
	else {
		iReturn = GLB_EOPN;
	}

	return iReturn;
}

int CGlbConf::GlbConfGetVal(char* pszSec, char* pszKey, char* pszVal)
{
	bool bSec = false;
	int iReturn = GLB_ESEC;
	char szLine[GLB_PACKET] = { 0 };

	if (NULL != m_pstFile) {
		rewind(m_pstFile);
		while (GLB_SUCCESS == GlbConfGetLine(szLine, GLB_PACKET - 1)) {
			if (true != GlbConfIsValid(szLine)) continue;
			if (true == GlbConfIsSection(szLine)) {
				if (false == bSec) {
					if (GLB_SUCCESS == strncasecmp(&szLine[1], pszSec, strlen(pszSec))) {
						iReturn = GLB_EKEY;
						bSec = true;
					}
				}
				else {
					bSec = false;
				}
			}
			else {
				if (true == bSec) {
					if (GLB_SUCCESS == strncasecmp(szLine, pszKey, strlen(pszKey))) {
						iReturn = GLB_EVAL;
						GlbConfTrimRight(szLine);
						if (GLB_SUCCESS == GlbConfKeyValue(szLine, pszKey, pszVal)) {
							iReturn = GLB_SUCCESS;
						}
						break;
					}
				}
			}
			memset(szLine, '\0', GLB_PACKET);
		}
	}
	else {
		iReturn = GLB_EOPN;
	}

	return iReturn;
}

int CGlbConf::GlbConfKeyValue(char* pszLine, char* pszKey, char* pszVal)
{
	char* pszCur = pszLine;
	int iReturn = GLB_SUCCESS;
	int iLength = strlen(pszLine);

	while (pszCur - pszLine < iLength && GLB_EQUAL != *pszCur) pszCur += 1;

	if (pszCur - pszLine != iLength) {
		*pszCur ++ = '\0';
		GlbConfTrimLeft(pszCur);
		strcpy(pszVal, pszCur);
	}
	else {
		iReturn = GLB_EVAL;
	}

	return iReturn;
}

int CGlbConf::GlbConfKeyValue(char* pszLine, char** ppszKey, char** ppszVal)
{
	char* pszCur = pszLine;
	int iReturn = GLB_SUCCESS;
	int iLength = strlen(pszLine);

	while (pszCur - pszLine < iLength && GLB_EQUAL != *pszCur) pszCur += 1;

	if (pszCur - pszLine != iLength) {
		*pszCur ++ = '\0';
		GlbConfTrimRight(pszLine);
		GlbConfTrimLeft(pszCur);
		*ppszKey = pszLine;
		*ppszVal = pszCur;
	}
	else {
		iReturn = GLB_EVAL;
	}

	return iReturn;
}

bool CGlbConf::GlbConfIsValid(char* pszLine)
{
	if (GLB_RCTARK != *pszLine) {
		GlbConfTrimLeft(pszLine);
		if (0 == strlen(pszLine)) {
			return false;
		}
	}
	else {
		return false;
	}

	return true;
}

bool CGlbConf::GlbConfIsSection(char* pszLine)
{
	if (GLB_SECL != *pszLine) {
		return false;
	}

	return true;
}

void CGlbConf::GlbConfTrim(char* pszLine)
{
	GlbConfTrimLeft(pszLine);
	GlbConfTrimRight(pszLine);

	return;
}

void CGlbConf::GlbConfTrimLeft(char* pszLine)
{
	char* pszCur = pszLine;

	while (GLB_GLBLT == *pszCur || GLB_GLBLR == *pszCur || GLB_GLBLN == *pszCur || GLB_BLANK == *pszCur) pszCur += 1;
	if (pszCur != pszLine) while ('\0' != (*pszLine ++ = *pszCur ++)) {}

	return;
}

void CGlbConf::GlbConfTrimRight(char* pszLine)
{
	char* pszCur = pszLine;

	if (strlen(pszLine) > 0) {
		while ('\0' != *pszCur && GLB_RCTARK != *pszCur) pszCur += 1;
		*pszCur -- = '\0';
		while (GLB_GLBLT == *pszCur || GLB_GLBLR == *pszCur || GLB_GLBLN == *pszCur || GLB_BLANK == *pszCur) pszCur -= 1;
		*(pszCur + 1) = '\0';
	}

	return;
}

} /* GlbCls */
