/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbDll.cpp			*/
/********************************************************/

#include "GlbDll.h"

namespace GlbCls
{

CGlbDll::CGlbDll()
{
	m_pDll = NULL;
}

CGlbDll::~CGlbDll()
{
	m_pDll = NULL;
}

void* CGlbDll::GlbDllGet()
{
	return m_pDll;
}

void CGlbDll::GlbDllClose()
{
	dlclose(m_pDll);

	return;
}

char* CGlbDll::GlbDllError()
{
	return dlerror();
}

void CGlbDll::GlbDllPut(void* pDll)
{
	m_pDll = pDll;

	return;
}

void* CGlbDll::GlbDllFunGet(char* pszFun)
{
	return dlsym(m_pDll, pszFun);
}

int CGlbDll::GlbDllOpen(char* pszFile, int iMode)
{
	int iReturn = GLB_SUCCESS;

	if(NULL == (m_pDll = dlopen(pszFile, iMode))) {
		GLB_ERROR("%s\n", GlbDllError());
		iReturn = GLB_FAILURE;
	}

	return iReturn;
}

} /* GlbCls */
