/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbFile.h			*/
/********************************************************/

#ifndef	_GLBFILE_H
#define	_GLBFILE_H

#include "GlbGlobal.h"
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

namespace GlbCls
{

#define	GLB_S_SET			SEEK_SET
#define	GLB_S_END			SEEK_END
#define	GLB_S_CUR			SEEK_CUR

#define	GLB_F_RDLCK			F_RDLCK
#define	GLB_F_WRLCK			F_WRLCK
#define	GLB_F_UNLCK			F_UNLCK

#define	GLB_F_SETLK			F_SETLK
#define	GLB_F_SETLKW			F_SETLKW

#define	GLB_O_EXCL			O_EXCL
#define	GLB_O_CREATE			O_CREAT

#define	GLB_O_RDWR			O_RDWR
#define	GLB_O_RDONLY			O_RDONLY
#define	GLB_O_WRONLY			O_WRONLY
#define	GLB_O_APPEND			O_APPEND

#define	GLB_P_READ			PROT_READ
#define	GLB_P_WRITE			PROT_WRITE

#define	GLB_M_SHARED			MAP_SHARED
#define	GLB_M_PRIVATE			MAP_PRIVATE

class CGlbFile
{
public:
	CGlbFile();
	virtual ~CGlbFile();

	void GlbFileClose();
	void* GlbFileGetFile();
	ULONG GlbFileGetSize();
	int GlbFileMunmap(ULONG ulSize);
	int GlbFileCreate(char* pszFile);
	int GlbFileDelete(char* pszFile);
	int GlbFileOpen(char* pszFile, int iFlag);
	int GlbFileTruncate(char* pszFile, ULONG ulSize);
	ULONG GlbFileLseek(short sWhence, ULONG ulOffset);
	int GlbFileMmap(int iProt, int iFlag, ULONG ulSize);
	int GlbFileFcntl(short sType, short sWhence, ULONG ulStart, ULONG ulLength, int iGlbd);

	virtual int GlbFileRead(UCHAR* puszLine, UINT uiLength, bool bProc);
	virtual int GlbFileWrite(UCHAR* puszLine, UINT uiLength, bool bProc);
	virtual void GlbFileMmapGet(ULONG ulOffset, UCHAR* puszLine, UINT uiLength, bool bProc);
	virtual void GlbFileMmapPut(ULONG ulOffset, UCHAR* puszLine, UINT uiLength, bool bProc);

protected:
	int m_iFile;
	void* m_pFile;
	ULONG m_ulSize;

private:

};

} /* GlbCls */

#endif /* _GLBFILE_H */
