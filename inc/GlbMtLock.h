/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMtLock.h			*/
/********************************************************/

#ifndef _GLBMTLOCK_H
#define	_GLBMTLOCK_H

#include "GlbLock.h"

namespace GlbCls
{

class CGlbMtLock : public CGlbLock
{
public:
	CGlbMtLock();
	virtual ~CGlbMtLock();

	virtual int GlbLockLock();
	virtual int GlbLockUnlock();
	virtual int GlbLockDelete();
	virtual int GlbLockCreate();
	virtual int GlbLockMalloc(void* pLock);
	virtual int GlbLockCreate(bool bShared);

protected:

private:

};

inline int CGlbMtLock::GlbLockLock()
{
	return pthread_mutex_lock((pthread_mutex_t*)CGlbLock::m_pLock);
}

inline int CGlbMtLock::GlbLockUnlock()
{
	return pthread_mutex_unlock((pthread_mutex_t*)CGlbLock::m_pLock);
}

} /* GlbCls */

#endif /* _GLBMTLOCK_H */
