/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbEpollTest.cpp		*/
/********************************************************/

#include "GlbTcp.h"
#include "GlbEpoll.h"

using namespace GlbNet;

int GlbEpollTestCore(int iSocket)
{
	CGlbTcp CTcp;
	char szSend[GLB_PACKET] = { 0 };
	char szRecv[GLB_PACKET] = { 0 };
	const char* pszHtml = "<html>\r\n<head>\r\n<title>Glbar</title>\r\n</head>\r\n<body>\r\n<p>This is CGlbEpoll test!</p>\r\n</body>\r\n</html>";

	CTcp.GlbNetSetSocket(iSocket);

	if (CTcp.GlbNetRecv((UCHAR*)szRecv, GLB_PACKET) > 0) {
		sprintf(szSend, "HTTP/1.1 200 OK\r\nContent-type: text/html\r\nContent-Length: %d\r\n%s\r\n\r\n", (int)strlen(pszHtml), pszHtml);
		CTcp.GlbNetSend((UCHAR*)szSend, strlen(szSend));
	}

	return GLB_SUCCESS;
}

int main(int argc, char* argv[])
{
	CGlbEpoll CEpoll;

	if (GLB_FAILURE == CEpoll.GlbEpollTcpInit((char*)"80")) {
		return GLB_FAILURE;
	}

	CEpoll.GlbEpollSetFunction(GlbEpollTestCore);
	CEpoll.GlbEpollTcpWait();

	return GLB_SUCCESS;
}
