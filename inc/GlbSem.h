/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbSem.h			*/
/********************************************************/

#ifndef _GLBSEM_H
#define	_GLBSEM_H

#include "GlbIpc.h"
#include <sys/sem.h>

namespace GlbIpc
{

typedef union tagGlbSem
{
	int m_iValue;
	struct semid_ds* m_pstBuf;
	USHORT* m_psArray;
}GLBSEM_U;

class CGlbSem : public CGlbIpc
{
public:
	CGlbSem();
	virtual ~CGlbSem();

	int GlbSemAcquire();
	int GlbSemRelease();
	int GlbSemAttach(key_t iKey);

	virtual int GlbIpcDelete();
	virtual int GlbIpcCreate(key_t iKey);

protected:

private:

};

} /* GlbIpc */

#endif /* _GLBSEM_H */
