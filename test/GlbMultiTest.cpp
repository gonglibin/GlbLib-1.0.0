/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbMultiTest.cpp		*/
/********************************************************/

#include "GlbMulti.h"

using namespace GlbNet;

void GlbMultiTestSend()
{
	int iCount = 0;
	CGlbMulti CSend;
	char szPacket[GLB_PACKET] = { 0 };

	if (GLB_SUCCESS == CSend.GlbNetInitSend((char*)"238.1.10.128", (char*)"12381")) {
		for (iCount = 0; iCount < 10; iCount ++) {
			sprintf(szPacket, "GlbMultiTestSend: %d\n", iCount);
			GLB_PRINT("Send: %s",szPacket);
			CSend.GlbNetSend((UCHAR*)szPacket, strlen(szPacket));
			memset(szPacket,'\0',GLB_PACKET);
			sleep(1);
		}

		CSend.GlbNetClose();
	}

	return;
}

void GlbMultiTestRecv()
{
	int iCount = 0;
	CGlbMulti CRecv;
	char szPacket[GLB_PACKET] = { 0 };

	if (GLB_SUCCESS == CRecv.GlbNetInitRecv((char*)"238.1.10.128", (char*)"12381")) {
		for (iCount = 0; iCount < 10; iCount ++) {
			if (CRecv.GlbNetRecv((UCHAR*)szPacket, GLB_PACKET) > 0) {
				GLB_PRINT("Recv: %s",szPacket);
				memset(szPacket,'\0',GLB_PACKET);
			}
		}

		CRecv.GlbNetClose();
	}

	return;
}

int main(int argc, char* argv[])
{
	if (argc < 2) {
		GLB_PRINT("Usage: %s [send|recv]\n", argv[0]);
		return GLB_SUCCESS;
	}

	if (GLB_SUCCESS == strncmp(argv[1], "send", strlen("send"))) {
		GlbMultiTestSend();
	}
	else if (GLB_SUCCESS == strncmp(argv[1], "recv", strlen("recv"))) {
		GlbMultiTestRecv();
	}
	else {
		GLB_PRINT("Parameter %s is error!\n", argv[1]);
	}

	return GLB_SUCCESS;
}
