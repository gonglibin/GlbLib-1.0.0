/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbGlobal.h			*/
/********************************************************/

#ifndef	_GLBGLOBAL_H
#define	_GLBGLOBAL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define	UINT				unsigned int
#define	UCHAR				unsigned char
#define	ULONG				unsigned long
#define	USHORT				unsigned short
#define	ULONGLONG			unsigned long long

#define	GLB_BYTE16			16
#define	GLB_BYTE32			32
#define	GLB_BYTE64			64

#define	GLB_BYTE128			128
#define	GLB_BYTE256			256
#define	GLB_BYTE512			512

#define	GLB_KBYTES1			1024
#define	GLB_KBYTES2			2048
#define	GLB_KBYTES4			4096
#define	GLB_KBYTES8			8192

#define	GLB_BUFFER			512
#define	GLB_PACKET			1024

#define	GLB_SUCCESS			0
#define	GLB_FAILURE			-1

#define	GLB_INFO(format, args...)	do {				\
	fprintf(stdout, "[INFO] %s %04d: ", __FILE__, __LINE__);	\
	fprintf(stdout, format, ##args);				\
} while (0)

#define	GLB_WARN(format, args...)	do {				\
	fprintf(stdout, "[WARN] %s %04d: ", __FILE__, __LINE__);	\
	fprintf(stdout, format, ##args);				\
} while (0)

#define	GLB_PRINT(format, args...)	do {				\
	fprintf(stdout, format, ##args);				\
	fflush(stdout);							\
} while (0)

#define	GLB_ERROR(format, args...)	do {				\
	fprintf(stderr, "[ERROR] %s %04d: ", __FILE__, __LINE__);	\
	fprintf(stderr, format, ##args);				\
} while (0)

#define	GLB_DEBUG(format, args...)	do {				\
	fprintf(stdout, "[DEBUG] %s %04d: ", __FILE__, __LINE__);	\
	fprintf(stdout, format, ##args);				\
	fflush(stdout);							\
} while (0)

#endif /* _GLBGLOBAL_H */
