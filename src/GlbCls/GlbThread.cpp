/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbThread.cpp			*/
/********************************************************/

#include "GlbThread.h"

namespace GlbCls
{

CGlbThread::CGlbThread()
{
	m_iID = 0;
	m_pRoutine = NULL;
	memset(&m_stAttr, '\0', sizeof(pthread_attr_t));
}

CGlbThread::~CGlbThread()
{
}

void CGlbThread::GlbThreadCore()
{
	return;
}

int CGlbThread::GlbThreadDestroy()
{
	if (GLB_SUCCESS != pthread_cancel(m_iID)) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbThread::GlbThreadCreate(void* pPara)
{
	if (GLB_FAILURE == pthread_create(&m_iID, &m_stAttr, m_pRoutine, pPara)) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbThread::GlbThreadJoin(void** ppReturn)
{
	if (GLB_SUCCESS != pthread_join(m_iID, ppReturn)) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbThread::GlbThreadAttrInit()
{
	if (GLB_SUCCESS != pthread_attr_init(&m_stAttr)) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

int CGlbThread::GlbThreadAttrSetDetach()
{
	if (GLB_SUCCESS != pthread_attr_setdetachstate(&m_stAttr, PTHREAD_CREATE_DETACHED)) {
		GLB_ERROR("%s\n", strerror(errno));
		return GLB_FAILURE;
	}

	return GLB_SUCCESS;
}

void CGlbThread::GlbThreadAttrSet(void* pAttr)
{
	memcpy((void*)&m_stAttr, pAttr, sizeof(pthread_attr_t));

	return;
}

GLB_ROUTINE CGlbThread::GlbThreadRoutineGet()
{
	return m_pRoutine;
}

void CGlbThread::GlbThreadRoutineSet(GLB_ROUTINE pRoutine)
{
	m_pRoutine = pRoutine;

	return;
}

} /* GlbCls */
