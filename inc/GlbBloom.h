/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbBloom.h			*/
/********************************************************/

#ifndef _GLBBLOOM_H
#define	_GLBBLOOM_H

#include "GlbGlobal.h"

#include <sys/ipc.h>
#include <sys/shm.h>

namespace GlbCls
{

#define	GLB_BLOOM_0			0x80
#define	GLB_BLOOM_1			0x40
#define	GLB_BLOOM_2			0x20
#define	GLB_BLOOM_3			0x10
#define	GLB_BLOOM_4			0x08
#define	GLB_BLOOM_5			0x04
#define	GLB_BLOOM_6			0x02
#define	GLB_BLOOM_7			0x01

class CGlbBloom
{
public:
	CGlbBloom();
	virtual ~CGlbBloom();

	void GlbBloomReset();
	void GlbBloomDelete();
	ULONG GlbBloomGetSize();
	UCHAR* GlbBloomGetBloom();
	int GlbBloomCreate(key_t iKey, ULONG ulSize);

	bool GlbBloomCheck(ULONGLONG ullKey);
	void GlbBloomUpdate(ULONGLONG ullKey);

protected:
	ULONG m_ulSize;
	UCHAR* m_puszBloom;

private:
	int m_iBloom;

};

inline bool CGlbBloom::GlbBloomCheck(ULONGLONG ullKey)
{
	UCHAR uszMask[] = {GLB_BLOOM_0, GLB_BLOOM_1, GLB_BLOOM_2, GLB_BLOOM_3, GLB_BLOOM_4, GLB_BLOOM_5, GLB_BLOOM_6, GLB_BLOOM_7};

	return uszMask[ullKey % 8] & *(m_puszBloom + ullKey % m_ulSize);
}

inline void CGlbBloom::GlbBloomUpdate(ULONGLONG ullKey)
{
	UCHAR* pucOffset = m_puszBloom + ullKey % m_ulSize;
	UCHAR uszMask[] = {GLB_BLOOM_0, GLB_BLOOM_1, GLB_BLOOM_2, GLB_BLOOM_3, GLB_BLOOM_4, GLB_BLOOM_5, GLB_BLOOM_6, GLB_BLOOM_7};

	*pucOffset |= uszMask[ullKey % 8];

	return;
}

} /* GlbCls */

#endif /* _GLBBLOOM_H */
