/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbSetTest.cpp			*/
/********************************************************/

#include "GlbSet.h"

using namespace GlbCls;

int GlbSetTestCompare(const void* m, const void* n)
{
	return *(ULONG*)m - *(ULONG*)n;
}

int main(int argc, char* argv[])
{
	CGlbSet CSet;
	CGlbSet CBak;
	ULONG ulIndex = 0;
	ULONG ulValue = 1;
	ULONG ulTotal = 100;
	ULONG* pulSet = NULL;

	if (GLB_SUCCESS == CSet.GlbSetInit(ulTotal, sizeof(ULONG))) {
		for (ulIndex = 0; ulIndex < ulTotal; ulIndex ++) {
			CSet.GlbSetIndexPut(ulIndex, &ulValue);
			ulValue += 1;
		}
	}

	CBak.GlbSetClone(CSet);
	GLB_PRINT("Erase[no] Order[no] -> Index[18]: %lu\n", *(ULONG*)CBak.GlbSetIndexGet(18));
	CBak.GlbSetIndexErase(18);
	GLB_PRINT("Erase[yes] Order[no] -> Index[18]: %lu\n", *(ULONG*)CBak.GlbSetIndexGet(18));
	CBak.GlbSetOrder();
	GLB_PRINT("Erase[yes] Order[yes] -> Index[18]: %lu\n", *(ULONG*)CBak.GlbSetIndexGet(18));

	CBak.GlbSetCompare(GlbSetTestCompare);
	CBak.GlbSetSort();

	if (NULL != (pulSet = (ULONG*)CBak.GlbSetGetSet())) {
		for (ulIndex = 0; ulIndex < ulTotal; ulIndex ++) {
			GLB_PRINT("Index[%04lu]: %lu\n", ulIndex, *(pulSet + ulIndex));
		}
	}

	ulIndex = 8;
	ulValue = 0;
	if (GLB_SUCCESS == CBak.GlbSetSearch(&ulIndex, &ulValue)) {
		GLB_PRINT("Key[%lu]: %lu\n", ulIndex, ulValue);
	}
	else {
		GLB_PRINT("Not find key[%lu]\n", ulIndex);
	}

	CSet.GlbSetDestroy();
	CBak.GlbSetDestroy();

	return GLB_SUCCESS;
}
