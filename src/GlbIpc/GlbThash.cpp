/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbThash.cpp			*/
/********************************************************/

#include "GlbThash.h"

namespace GlbIpc
{

CGlbThash::CGlbThash()
{
}

CGlbThash::~CGlbThash()
{
}

void CGlbThash::GlbThashDelete()
{
	m_CLock.GlbLockDelete();
	CGlbHash::GlbHashDelete();

	return;
}

int CGlbThash::GlbThashCreate(key_t iKey, ULONG ulHash)
{
	int iReturn = GLB_SUCCESS;

	if (GLB_SUCCESS == m_CLock.GlbLockMalloc(NULL)) {
		if (GLB_SUCCESS == m_CLock.GlbLockCreate(false)) {
			if (GLB_SUCCESS != CGlbHash::GlbHashCreate(iKey, ulHash)) {
				iReturn = GLB_FAILURE;
			}
		}
		else {
			iReturn = GLB_FAILURE;
		}
	}
	else {
		iReturn = GLB_FAILURE;
	}

	return iReturn;
}

} /* GlbIpc */
