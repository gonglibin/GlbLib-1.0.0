/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbCodeTest.cpp			*/
/********************************************************/

#include "GlbCode.h"

int main(int argc, char* argv[])
{
	char *pszUTF8 = NULL;
	char *pszChinese = NULL;
	char *pszGB2312 = NULL;
	char szTest[64] = "中国人民解放军";

	/* Chinese to GB2312 */
	pszGB2312 = GlbCodeChineseToGB2312(szTest);
	GLB_PRINT("GB2312: %s -> Length: %d\n", pszGB2312, strlen(pszGB2312));

	/* GB2312 to Chinese */
	pszChinese = GlbCodeGB2312ToChinese(pszGB2312);
	GLB_PRINT("Chinese: %s -> Length: %d\n", pszChinese, strlen(pszChinese));

	/* Chinese to UTF8 */
	pszUTF8 = GlbCodeChineseToUTF8(pszChinese);
	GLB_PRINT("UTF8: %s -> Length: %d\n", pszUTF8, strlen(pszUTF8));

	/* UTF8 to Chinese */
	pszChinese = GlbCodeUTF8ToChinese(pszUTF8);
	GLB_PRINT("Chinese: %s -> Length: %d\n", pszChinese, strlen(pszChinese));

	if (NULL != pszGB2312) free(pszGB2312);
	if (NULL != pszChinese) free(pszChinese);
	if (NULL != pszUTF8) free(pszUTF8);

	return GLB_SUCCESS;
}
