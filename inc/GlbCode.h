/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbCode.h			*/
/********************************************************/

#ifndef	_GLBCODE_H
#define	_GLBCODE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "GlbGlobal.h"

#include <ctype.h>
#include <iconv.h>

char GlbCodeCharToInt(char ch);
char GlbCodeStrToBin(char* pszWord);
char* GlbCodeUTF8ToChinese(char* pszUTF8);
char* GlbCodeChineseToUTF8(char* pszChinese);
char* GlbCodeGB2312ToChinese(char* pszGB2312);
char* GlbCodeChineseToGB2312(char* pszChinese);
int GlbCodeConvert(char* pszFm, char* pszTo, char* pszSrc, int iSrc, char* pszDst, int iDst);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _GLBCODE_H */
