/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbLogTest.cpp			*/
/********************************************************/

#include "GlbLog.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbLog CLog;
	char szLine[GLB_PACKET] = { 0 };

	if (GLB_SUCCESS == CLog.GlbLogOpen((char*)"GlbLogTest.log", GLB_LOG_APPEND)) {
		sprintf(szLine, "%s %d: GlbLogTest.\n", __FILE__, __LINE__);
		CLog.GlbLogWrite(szLine, strlen(szLine));
		CLog.GlbLogClose();
	}

	return GLB_SUCCESS;
}
