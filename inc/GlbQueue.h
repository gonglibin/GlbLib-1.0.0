/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbQueue.h			*/
/********************************************************/

#ifndef	_GLBQUEUE_H
#define	_GLBQUEUE_H

#include "GlbGlobal.h"
#include <stdexcept>
#include <pthread.h>

namespace GlbCls
{

#define	GLB_QUEUE			1000000

class CGlbQueue
{
public:
	CGlbQueue();
	CGlbQueue(bool bMulti);
	virtual ~CGlbQueue();

	ULONG GlbQueueGetTotal();
	void* GlbQueueHasNext(void* pCur);
	int GlbQueuePop(void* pData, UINT uiSize);
	int GlbQueuePut(void* pData, UINT uiSize);
	int GlbQueueGet(void* pData, UINT uiSize);

protected:
	bool m_bMulti;
	void* m_pHead;
	void* m_pTail;
	ULONG m_ulTotal;

	pthread_cond_t m_stCond;
	pthread_mutex_t m_stLock;

private:
	void GlbQueueLock();
	void GlbQueueUnlock();
	void* GlbQueueNew(void* pData, UINT uiSize);

};

inline void CGlbQueue::GlbQueueLock()
{
	if (GLB_SUCCESS != pthread_mutex_lock(&m_stLock)) throw(strerror(errno));

	return;
}

inline void CGlbQueue::GlbQueueUnlock()
{
	if (GLB_SUCCESS != pthread_mutex_unlock(&m_stLock)) throw(strerror(errno));

	return;
}

} /* GlbCls */

#endif /* _GLBQUEUE_H */
