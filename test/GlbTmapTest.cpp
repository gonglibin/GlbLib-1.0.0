/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbTmapTest.cpp			*/
/********************************************************/

#include "GlbTmap.h"
#include "GlbTime.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbTime CTime;
	ULONG ulKey = 0;
	ULONG ulValue = 0;
	CGlbTmap<ULONG, ULONG, GLBMAPDIGIT_O> CTmap;

	if (GLB_SUCCESS == CTmap.GlbTmapCreateLock()) {
		/* insert */
		CTime.GlbTimeOn();
		for (; ulKey < 1000000; ulKey ++) {
			CTmap.GlbTmapInsert(ulKey, ulValue);
			ulValue += 1;
		}
		CTime.GlbTimeOff();
		CTime.GlbTimeDisplay();

		//CTmap.GlbTmapDisplay();

		/* select */
		ulKey = 999;
		ulValue = 0;
		CTime.GlbTimeOn();
		CTmap.GlbTmapSelect(ulKey, ulValue, sizeof(ULONG));
		CTime.GlbTimeOff();
		CTime.GlbTimeDisplay();
		GLB_PRINT("Select { Key: %ld Value: %ld }\n", ulKey, ulValue);

		CTmap.GlbTmapDeleteLock();
	}
	else {
		GLB_ERROR("Failed to GlbTmapCreateLock\n");
	}

	return GLB_SUCCESS;
}
