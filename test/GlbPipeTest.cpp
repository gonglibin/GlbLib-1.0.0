/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbPipeTest.cpp			*/
/********************************************************/

#include "GlbPipe.h"

using namespace GlbCls;

int main(int argc, char* argv[])
{
	CGlbPipe CPipe;
	char szGlbd[GLB_PACKET] = { 0 };

	if (NULL != argv[1]) {
		strcpy(szGlbd, argv[1]);
	}
	else {
		strcpy(szGlbd, "mpstat");
	}

	if (GLB_SUCCESS == CPipe.GlbPipeOpen(szGlbd, (char*)"r")) {
		if (GLB_SUCCESS == CPipe.GlbPipeResultMalloc(GLB_PIPE)) {
			if (CPipe.GlbPipeRead(GLB_PIPE) > 0) {
				GLB_PRINT("Result:\n%s", CPipe.GlbPipeResultGet());
			}
			else {
				GLB_PRINT("Failed to GlbPipeTest!\n");
			}
			CPipe.GlbPipeResultFree();
		}
		CPipe.GlbPipeClose();
	}

	return GLB_SUCCESS;
}
