/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbUdp.h			*/
/********************************************************/

#ifndef _GLBUDP_H
#define	_GLBUDP_H

#include "GlbNet.h"
#define __FAVOR_BSD
#include <netinet/udp.h>

namespace GlbNet
{

class CGlbUdp : public CGlbNet
{
public:
	CGlbUdp();
	virtual ~CGlbUdp();

	void GlbUdpReset();
	void* GlbUdpGetAddr();
	void GlbUdpSetAddr(void* pAddr);
	void GlbUdpSetAddr(char* pszUrl);
	int GlbUdpConnect(char* pszUrl);
	int GlbUdpConnect(struct sockaddr_in* pstAddr);
	int GlbUdpConnect(char* pszAddr, char* pszPort);

	int GlbUdpRead(UCHAR* puszPacket, UINT uiLength);
	int GlbUdpWrite(UCHAR* puszPacket, UINT uiLength);

	virtual int GlbNetSend(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecv(UCHAR* puszPacket, int iLength);

	virtual int GlbNetSendAll(UCHAR* puszPacket, int iLength);
	virtual int GlbNetRecvAll(UCHAR* puszPacket, int iLength);

	virtual int GlbNetInitSend(char* pszUrl);
	virtual int GlbNetInitRecv(char* pszUrl);
	virtual int GlbNetInitSend(struct sockaddr_in* pstAddr);
	virtual int GlbNetInitRecv(struct sockaddr_in* pstAddr);
	virtual int GlbNetInitSend(char* pszAddr, char* pszPort);
	virtual int GlbNetInitRecv(char* pszAddr, char* pszPort);

protected:
	struct sockaddr_in m_stAddr;

private:

};

inline int CGlbUdp::GlbUdpRead(UCHAR* puszPacket, UINT uiLength)
{
	return read(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbUdp::GlbUdpWrite(UCHAR* puszPacket, UINT uiLength)
{
	return write(CGlbNet::m_iSocket, puszPacket, uiLength);
}

inline int CGlbUdp::GlbNetSend(UCHAR* puszPacket, int iLength)
{
	return sendto(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stAddr, sizeof(struct sockaddr_in));
}

inline int CGlbUdp::GlbNetRecv(UCHAR* puszPacket, int iLength)
{
	int iUdp = sizeof(struct sockaddr_in);

	return recvfrom(CGlbNet::m_iSocket, puszPacket, iLength, 0, (struct sockaddr*)&m_stAddr, (socklen_t*)&iUdp);
}

} /* GlbNet */

#endif /* _GLBUDP_H */
