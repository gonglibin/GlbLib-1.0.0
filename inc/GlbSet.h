/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbSet.h			*/
/********************************************************/

#ifndef _GLBSET_H
#define	_GLBSET_H

#include "GlbGlobal.h"

namespace GlbCls
{

typedef int (*GlbSetCompPtr)(const void*, const void*);

class CGlbSet
{
public:
	CGlbSet();
	virtual ~CGlbSet();

	void* GlbSetGetSet();
	ULONG GlbSetGetSize();
	ULONG GlbSetGetCount();
	GlbSetCompPtr GlbSetGetCompare();

	void GlbSetPutSet(void* pSet);
	void GlbSetPutSize(ULONG ulSize);
	void GlbSetPutCount(ULONG ulCount);

	void GlbSetSort();
	void GlbSetUniq();
	void GlbSetOrder();
	void GlbSetDestroy();
	long GlbSetIndex(void* pKey);
	void GlbSetClone(CGlbSet& CSet);
	void* GlbSetSearch(void* pKey);
	void GlbSetReset(ULONG ulTotal);
	void* GlbSetIndexGet(ULONG ulIndex);
	void GlbSetIndexErase(ULONG ulIndex);
	bool GlbSetIndexIsNull(UCHAR* pucCur);
	void GlbSetCompare(GlbSetCompPtr pCompare);
	int GlbSetSearch(void* pKey, void* pData);
	int GlbSetInit(ULONG ulTotal, ULONG ulSize);
	void GlbSetReset(ULONG ulTotal, ULONG ulSize);
	ULONG GlbSetIndexPut(ULONG ulIndex, void* pData);

protected:
	void* m_pSet;
	ULONG m_ulSize;
	ULONG m_ulCount;
	GlbSetCompPtr m_pCompare;

private:

};

} /* GlbCls */

#endif /* _GLBSET_H */
