/********************************************************/
/*	Copyright (C) 2016 Gong Li Bin		 	*/
/*	Project:	GlbLib-1.0.0			*/
/*	Author:		gong_libin			*/
/*	Date:		2016_06_01			*/
/*	File:		GlbAcbmTest.cpp			*/
/********************************************************/

#include "GlbAcbm.h"
#include "GlbTime.h"

using namespace GlbCls;

#define	GLB_TEST				10
#define	GLB_DATA				1000

int main(int argc, char* argv[])
{
	CGlbAcbm CAcbm;
	CGlbTime CTime;
	int iMatch = 0;
	int iCount = 0;
	char szKey[] = "789";
	char szText[] = "123456789abcdefg";
	GLBACBMDATA_S* pstData = NULL;
	GLBACBMMATCH_S stMatch[GLB_TEST];

	GLB_PRINT("\n--- Text ---\n");
	GLB_PRINT("%s\n", szText);

	memset(stMatch, '\0', sizeof(stMatch));
	if (NULL != (pstData = (GLBACBMDATA_S*)GlbMalloc(sizeof(GLBACBMDATA_S) * GLB_DATA))) {
		for (iCount = 0; iCount < GLB_DATA; iCount ++) {
			(pstData + iCount)->m_iLength = GLB_TEST;
			memset((pstData + iCount)->m_uszData, 'A', GLB_TEST);
		}

		(pstData + iCount - 5)->m_iLength = strlen(szKey);
		strcpy((char*)(pstData + iCount - 5)->m_uszData, szKey);

		CTime.GlbTimeOn();
		GLB_PRINT("\n--- TreeInit ---\n");
		CAcbm.GlbAcbmTreeInit(pstData, GLB_DATA);
		//CAcbm.GlbAcbmDisplayKey(pstData, GLB_DATA);
		CTime.GlbTimeOff();
		CTime.GlbTimeDisplay();

		CTime.GlbTimeOn();
		GLB_PRINT("\n--- TreeMatch ---\n");
		iMatch = CAcbm.GlbAcbmSearch((UCHAR*)szText, strlen(szText), stMatch, GLB_TEST);
		CAcbm.GlbAcbmDisplayMatch(pstData, stMatch, iMatch);
		CTime.GlbTimeOff();
		CTime.GlbTimeDisplay();

		CAcbm.GlbAcbmClean();

		GlbFree((void**)&pstData);
	}

	return GLB_SUCCESS;
}
